<?php
require_once 'php/seguridad.php';

if(!isset($_SESSION)) {session_start();}
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Conquistadores</title>
    <!--link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/"-->
    <!-- Bootstrap core CSS -->
		<link href="assets/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/peniaflor.css" rel="stylesheet">
  </head>
  <body>
    <?php  
			include_once('cabecera.php');
		?>
		<main role="main" style="margin-bottom: 4.3rem;">

			<section class="gp-balance">
				<div class="container">
				<h2>El programa</h2>
					<div class="row gp-elprograma">
						<div class="col-lg-4">
							<img src="img/logo-negro.png" width="100%" alt="El programa"/></div> 
						<div class="col-lg-8">
							<h4 style="font-size: 25px; margin-bottom: 0px; color:black; ">Llegó la Conquista del Año, ¿están preparados para el desafío?</h4>
							<h3 style="font-size: 25px; margin-bottom: 0px; ">¿El objetivo?</h3>
							
								<ul class="">
									<li>Metas comerciales: CCC y volumen.</li>
									<li>Ejecución de PDVs.</li>
									<li>Proceso Full Star</li>
								</ul>

							<h3 style="font-size: 25px; margin-bottom: 0px; ">¿Quiénes participan?</h3>
							
							<div class="gp-franja-lineas"></div>
								<ul class="quienes">
								<li>300 vendedores</li>
								<li>34 distribuidores</li>
								<li>11 KAS</li>
								<li>5 KAM</li>
							</ul>
							<div class="gp-franja-lineas"></div>
							<h3 style="font-size: 25px; margin-bottom: 0px; ">¿Como será la estructura del incentivo?</h3>
						
							<ul>
								<li>Compiten AMBA e Interior por separado
									<ul>
										<li>Los distris de AMBA compiten entre sí mismos, ranking por vendedor.</li>
										<li>Los distris de Interior compiten todos los vendedores en conjunto, ranking por región.</li>
										<li>Cada mes tendremos distintos SKUs foco.</li>
									</ul>
								</li>
								<li>Para cada SKU participante tendrás un objetivo de CCC y de volumen.</li>
								<li>A su vez, las ejecuciones que realices de los SKUs del mes suman puntos extras.</li>
								<li>De acuerdo al alcance de ese objetivo, se te asignará un puntaje.</li>
								<li>La premiación es trimestral, con avances mensuales</li>
							</ul>
							<h3 style="font-size: 25px; margin-bottom: 0px; ">¿Cómo será la puntuación?</h3>
							<p>Vendedores:</p>

							<img src="img/image003.png" width="100%" alt="conquistadores"/>

							<p>Supervisores, KAS y KAM: obtienen el promedio de puntos de su equipo + procesos Full Star.</p>

								
							 </div> 
					</div>
					<!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
		<script src="assets/dist/js/bootstrap.bundle.js"></script>

		<?php include_once('pie.php');?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171840062-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-171840062-1');
		</script>
	</body>
</html>
