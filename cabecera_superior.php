
<!-- dfs ******************************************************************************* -->
<!-- <div class="container-grid bg-info">

	<div class="logo-grid bg-danger text-center">
		<a class="navbar-brand" href="home.php"><img class="logo" src="img/Logos-Nestle.png" alt="Conquistadores 2021"/></a>
	</div>

	<nav class="nav1 navbar navbar-expand-md navbar-dark ">
		<button class="navbar-toggler button-hamb-grid" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-end mr-5" id="navbarCollapse">
			<ul class="navbar-nav ">
				<li class="nav-item active">
					<a class="nav-link" href="home.php">INICIO</a>
				</li>
				<li class="nav-item"> <a class="nav-link" href="el_programa.php">EL PROGRAMA</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="mi_performance.php">MI PERFORMANCE</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="novedades.php">NOVEDADES</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="contacto.php">CONTACTO</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="nav2 d-md-flex justify-content-end align-items-center pr-md-5 text-center">

		<div class="mr-4 text-center" style="">
			<p><--?php echo strtoupper($_SESSION['QLMSF_rango']);?></p>
			<p><--?php echo $_SESSION['QLMSF_nombre'].' '.$_SESSION['QLMSF_apellido'];?></p>
			<h2><--?php echo round($_SESSION['QLMSF_puntos'], 1);?> puntos</h2>
		</div>
		<div class="mr-4 d-flex text-center" style="justify-content: space-around;">
			<a href="datos_personales.php">
				<div><img class="conquistadores-accesos" src="img/asset.png" alt="Datos personales"/></div>
				<small>DATOS PERSONALES</small>
			</a>

			<a href="balance_millas.php">
				<div><img class="conquistadores-accesos" src="img/asset4.png"  alt="Balance de puntos"/></div>
				<small>MI BALANCE</small>
			</a>
		</div>
		<div class="mr-4">
			<a href="php/logout.php" class="gp-btn-rojo-borde">CERRAR&#160;SESIÓN</a>
		</div>
				
	</div>
</div> -->

<!-- /***************************** */ -->

<div class="container-grid bg-gp2020 sticky-top">

	<div class="logo-grid text-center">
		<a class="navbar-brand" href="home.php"><img class="logo" src="img/Logos-Nestle.png" alt="Conquistadores 2021"/></a>
	</div>

 	<div class="balance-grid">
  		<div class="text-center" style="margin:1rem;">
			<p  class="p-md-0 m-md-0"><?php echo strtoupper($_SESSION['QLMSF_rango']);?></p>
			<p  class="p-md-0 m-md-0"><?php echo $_SESSION['QLMSF_nombre'].' '.$_SESSION['QLMSF_apellido'];?></p>
			<h2 class="p-md-0 m-md-0"><?php echo round($_SESSION['QLMSF_puntos'], 1);?> puntos</h2>
		</div>
 	</div>

	<nav class="nav1-grid navbar navbar-expand-md navbar-dark ">
		<button class="navbar-toggler button-hamb-grid" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-start" id="navbarCollapse">
			<ul class="navbar-nav ">
				<li class="nav-item active">
					<a class="nav-link" href="home.php">INICIO</a>
				</li>
				<li class="nav-item"> <a class="nav-link" href="el_programa.php">EL PROGRAMA</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="mi_performance.php">MI PERFORMANCE</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="novedades.php">NOVEDADES</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="contacto.php">CONTACTO</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="nav2-grid d-md-flex justify-content-end align-items-center  text-center">

		<div class="ml-md-3 mr-md-4 d-flex text-center" style="justify-content: space-around;">

			<!-- <div class="misdato-grid-icon">
				<a href="datos_personales.php">
					<div><img class="conquistadores-accesos" src="img/asset.png" alt="Datos personales"/></div>
					<small>DATOS PERSONALES</small>
				</a>
			</div> -->

			<a class="text-white a-grid" href="datos_personales.php" style="text-decoration: none;">
				<div class=" mr-md-5" style="display: flex;align-items: center;justify-content: center;"><img class="conquistadores-accesos misdato-grid-icon" src="img/asset.png" alt="Datos personales"/><small class="d-none d-md-block">DATOS PERSONALES</small></div>
				<small class="d-md-none">DATOS PERSONALES</small>
			</a>

			<a class="text-white a-grid" href="balance_millas.php" style="text-decoration: none;">
				<div class=" mr-md-5" style="display: flex;align-items: center;justify-content: center;"><img class="conquistadores-accesos misdato-grid-icon" src="img/asset4.png"  alt="Balance de puntos"/><small class="d-none d-md-block">MI BALANCE</small></div>
				<small class="d-md-none">MI BALANCE</small>
			</a>
		</div>
		<div class=" mr-4">
			<a href="php/logout.php" class="gp-btn-rojo-borde">CERRAR&#160;SESIÓN</a>
		</div>
		
	</div>

</div>