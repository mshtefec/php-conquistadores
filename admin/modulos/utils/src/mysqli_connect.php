<?php
    include_once("../../php/class/configuracion.php");

    session_start();

    $host = DDBBHOST;
    $dbname = DDBBDATABASE;
    $user = DDBBUSER;
    $pass = DDBBPASSWORD;
    $locale = 'es_AR';
    
    try {
        $conn = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8', $user, $pass, [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET lc_time_names='" . $locale . "'" 
        ]);
        $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch(PDOException $e) {
        echo "<br>" . $e->getMessage();
    }
?>