<?php

interface RepositoryInterface
{
    // select
    public function find($id);

    // create - update
    public function save(Socio $obj);

    // delete
    public function remove(Socio $obj);
}