<?php 

    session_start();

    include('scripts/db.php');

    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $stmt = $dbh->prepare("
            SELECT 
                g.idsocio as id,
                g.concepto as concepto,
                g.ranking as ranking,
                g.puntaje as puntaje,
                CONCAT(s.apellido, ', ', s.nombre) as nombre,
                s.region as region,
                s.rango as rango,
                DATE_FORMAT(g.fecha, '%d-%m-%Y') as fecha
            FROM 
                ganadores as g
            JOIN
                socios as s ON (g.idsocio = s.idsocio)
            WHERE 
                s.idsocio = :id
        ");

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();
        $socio = $stmt->fetch(PDO::FETCH_ASSOC);
    }

?>


<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Detalles Ganador
                    <a type="button" class="btn btn-primary" href="index.php"><i class="bi bi-arrow-return-left"></i> Volver a Ganadores</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Detalles Ganador</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Información</h6>
                    
                    <form>
                        <hr class="my-4">

                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="idsocio" class="form-label">Socio</label>
                                <input type="text" class="form-control" id="nombre" value="<?php echo $socio['nombre'] ?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="region" class="form-label">Region</label>
                                <input type="text" class="form-control" id="region" value="<?php echo $socio['region'] ?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="rango" class="form-label">Rango</label>
                                <input type="text" class="form-control" id="rango" value="<?php echo $socio['rango'] ?>" readonly>
                            </div>
                            <div class="col-md-2 offset-1">
                                <label for="periodo" class="form-label">Periodo</label>
                                <input type="text" class="form-control" id="periodo" value="<?php echo $socio['fecha'] ?>" readonly>
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-2">
                                <label for="concepto" class="form-label">Concepto</label>
                                <input type="text" class="form-control" id="concepto" value="<?php echo $socio['concepto'] ?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="ranking" class="form-label">Ranking</label>
                                <input type="text" class="form-control" id="ranking" value="<?php echo $socio['ranking'] ?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="puntaje" class="form-label">Puntaje</label>
                                <input type="number" class="form-control" id="puntaje" value="<?php echo $socio['puntaje'] ?>" readonly>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>

<script>
    $( function() {
        $("#periodo").datepicker({
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            //dateFormat: "yy-mm-dd"
        });
    } );

    function changeInfo(x) {
        let json = <?php echo json_encode($socios) ?>;
        const socio = json.find(element => element.id > x);
        $("#region").val(socio.region);
        $("#rango").val(socio.rango);
    };
</script>