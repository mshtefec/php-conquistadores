<?php 

    session_start();

    include('scripts/db.php');

    $socios = array();

    $stmt = $dbh->prepare("
        SELECT 
            s.idsocio as id,
            CONCAT(s.apellido, ', ', s.nombre) as nombre,
            region,
            rango
        FROM 
            socios as s
        WHERE
            rango <> 'vendedor'
    ");
    $stmt->execute();
    $socios = $stmt->fetchAll(PDO::FETCH_ASSOC);

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $idsocio = $_POST['idsocio'];
        $periodo = $_POST['periodo'];
        $newDate = date("Y-m-d", strtotime($periodo));  
        //$fecha = date_create_from_format('Y-m-d', $newDate);

        $sql = "INSERT INTO users (name, surname, sex) VALUES (?,?,?)";
        $stmt = $dbh->prepare("
            INSERT INTO ganadores (idsocio, fecha, concepto, ranking, puntaje) VALUES (?,?,?,?,?)
        ");

        $stmt->execute([$idsocio, $newDate]);

        header("Location: index.php");

    }

?>


<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Nuevo de Ganador
                    <a type="button" class="btn btn-primary" href="index.php"><i class="bi bi-arrow-return-left"></i> Volver a Ganadores</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Nuevo Ganador</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Nuevo de Ganador</h6>
                    
                    <form action="new.php" method="post">
                        <hr class="my-4">

                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="idsocio" class="form-label">Id Socio</label>
                                <select class="form-select" id="idsocio" name="idsocio" required="" onchange="changeInfo(this.value)">
                                    <option value="">Busca el ganador...</option>
                                    <?php foreach ($socios as $socio) { ?>
                                        <option value="<?php echo $socio['id'] ?>"><?php echo $socio['nombre']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="region" class="form-label">Region</label>
                                <input type="text" class="form-control" id="region" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="rango" class="form-label">Rango</label>
                                <input type="text" class="form-control" id="rango" readonly>
                            </div>
                            <div class="col-md-2 offset-1">
                                <label for="periodo" class="form-label">Mes</label>
                                <input type="text" class="form-control" id="periodo" name="periodo">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-2">
                                <label for="concepto" class="form-label">Concepto</label>
                                <input type="text" class="form-control" id="concepto" name="concepto">
                            </div>
                            <div class="col-md-2">
                                <label for="ranking" class="form-label">Ranking</label>
                                <select class="form-select" id="ranking" name="ranking">
                                    <?php for ($i = 1; $i <= 17; $i++) { ?>
                                        <option value="<?php echo $i ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <label for="puntaje" class="form-label">Puntaje</label>
                                <input type="number" class="form-control" id="puntaje" name="puntaje">
                            </div>
                        </div>

                        <hr class="my-4">

                        <button class="w-100 btn btn-success btn" type="submit">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>

<script>
    $( function() {
        $("#periodo").datepicker({
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            //dateFormat: "yy-mm-dd"
        });
    } );

    function changeInfo(x) {
        let json = <?php echo json_encode($socios) ?>;
        const socio = json.find(element => element.id > x);
        $("#region").val(socio.region);
        $("#rango").val(socio.rango);
    };
</script>