<?php

	//me fijo el mes o viene dado o es el actual
	if(isset($_GET['m']) && strpos($_GET['m'], '-') !== false){
		$mes = explode('-', $_GET['m'])[0];
		$anio = explode('-', $_GET['m'])[1];
	} else {
		if(isset($_GET['m']) && is_numeric($_GET['m'])) {$mes = $_GET['m'];} else {$mes = intval(date('m'));}
		$anio = date('Y');
	}

	if(isset($_SESSION['socio'])) {
		$id = $_SESSION['socio'];
		
		$puntosSrv = new PuntosService();
		
		$puntos_by_region = $puntosSrv->getPuntosMonthBySocioGroupByRegion($id, $mes, "ejecucion");
		$puntos_by_distribuidor = $puntosSrv->getPuntosMonthBySocioGroupByDistribuidor($id, $mes, "ejecucion");
		$puntos_by_vendedor = $puntosSrv->getPuntosMonthBySocioGroupByVendedor($id, $mes, "ejecucion");
	}

?>

<section class="gp-ranking-distri">

	<div class="row">
		<div class="col-lg-11">
			
			<h2>Objetivos <?php echo $nombre_mes[$mes] . " " . $anio?></h2>
			
			<?php if ($_SESSION['QLMSF_rango'] == "vendedor") {?>
				<ul class="nav nav-tabs">
					<li id="distribuidor_solapa" class="active"><a onclick="showDistribuidor();" href="javascript:void(1);">Por Vendedor</a></li>
				</ul>
			<?php } else if ($_SESSION['QLMSF_rango'] == "supervisor") {?>
				<ul class="nav nav-tabs">
					<li id="linea_solapa" class="active"><a onclick="showLinea();" href="javascript:void(1);">Por Distribuidor</a></li>
					<li id="distribuidor_solapa"><a onclick="showDistribuidor();" href="javascript:void(1);">Por Vendedor</a></li>
				</ul>
			<?php } else {?>
				<ul class="nav nav-tabs">
					<li id="region_solapa" class="active"><a onclick="showRegion();" href="javascript:void(1);">Por Regi&oacuten</a></li>
					<li id="linea_solapa"><a onclick="showLinea();" href="javascript:void(1);">Por Distribuidor</a></li>
					<li id="distribuidor_solapa"><a onclick="showDistribuidor();" href="javascript:void(1);">Por Vendedor</a></li>
				</ul>
			<?php }?>

			<!--Objetivos POR REGION-->
			<div id="region_contenido" class="row" id="por-region" style="padding-top:20px; display:block">
				<div class="col-lg-12">
					
					<table class="table table-sm">
						<thead class="thead-light">
							<tr>
								<th width="12%">Regi&oacuten</th>
								<th width="28%">Producto</th>
								<th width="12%">Objetivo</th>
								<th width="12%">CCC</th>
								<th width="30%">Porcentaje</th>
								<th width="6%">%</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$total_obj_r = 0;
								$total_ccc_r = 0;

								$acum_obj_r = 0;
								$acum_ccc_r = 0;
							?>
							<?php foreach ($puntos_by_region as $puntos) { ?>
								<?php 
									$total_obj_r = $puntos->objetivo;
									$total_ccc_r = $puntos->ccc;

									$acum_obj_r += $total_obj_r;
									$acum_ccc_r += $total_ccc_r;

									$color = "verde";
									$percentage = 0;

									switch ($puntos->per) {
										case $puntos->per < 75:
											$color = "rojo";
											$percentage = round($total_ccc_r / $total_obj_r * 100, 1);
											break;
										case ($puntos->per >= 75 && $puntos->per <= 95):
											$color = "amarillo";
											$percentage = round($total_ccc_r / $total_obj_r * 100, 1);
											break;
										case ($puntos->per > 95 && $puntos->per <= 100):
											$color = "verde";
											$percentage = round($total_ccc_r / $total_obj_r * 100, 1);
											break;
										case $puntos->per >= 100:
											$color = "verde";
											$percentage = round($total_ccc_r / $total_obj_r * 100, 1);
											break;
									}
								?>
								<tr>
									<td><?php if($puntosegion_prima != $puntos->region) { echo "<strong>". $puntos->region ."</strong>"; $puntosegion_prima = $puntos->region; } ?></td>
									<td><?php echo  $puntos->linea ?></td>
									<td><?php echo  $puntos->objetivo ?></td>
									<td><?php echo  $puntos->ccc ?></td>
									<td>
										<div class="barra">
											<?php if($percentage > 100) { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="100%" height="15" alt="100%"/>
											<?php } else { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="<?php echo $percentage ?>%" height="15" alt="100%"/>
											<?php } ?>
										</div> 
									</td>
									<td><?php echo  $percentage ?>%</td>
								</tr>
							<?php } ?>
							
							<tr>
								<td colspan="2">TOTAL GENERAL</td>
								<td><?php echo $acum_obj_r?></td>
								<td><?php echo $acum_ccc_r?></td>
								<td>
									<div class="barra">
										<img src="img/barra-verde.png" width="100%" height="15" alt="100%"/>
									</div> 
								</td>
								<td><?php echo round($acum_ccc_r / $acum_obj_r * 100, 1); ?>%</td>
							</tr>
						</tbody>
					</table>	
					
				</div>
			</div>

			<!--Objetivos POR DISTRIBUIDOR-->
			<div id="linea_contenido" class="row" style="padding-top:20px; display:none">
				<div class="col-lg-12">
					
					<table class="table table-sm">
						<thead class="thead-light">
							<tr>
								<th width="12%">Distribuidor</th>
								<th width="28%">Producto</th>
								<th width="12%">Objetivo</th>
								<th width="12%">CCC</th>
								<th width="30%">Porcentaje</th>
								<th width="6%">%</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$total_obj_d = 0;
								$total_ccc_d = 0;

								$acum_obj_d = 0;
								$acum_ccc_d = 0;
							?>
							<?php foreach ($puntos_by_distribuidor as $puntos) { ?>
								<?php 
									$total_obj_d = $puntos->objetivo;
									$total_ccc_d = $puntos->ccc;
									
									$acum_obj_d += $total_obj_d;
									$acum_ccc_d += $total_ccc_d;

									$color = "verde";
									$percentage = 0;

									switch ($puntos->per) {
										case $puntos->per < 75:
											$color = "rojo";
											$percentage = round($total_ccc_d / $total_obj_d * 100, 1);
											break;
										case ($puntos->per >= 75 && $puntos->per <= 95):
											$color = "amarillo";
											$percentage = round($total_ccc_d / $total_obj_d * 100, 1);
											break;
										case ($puntos->per > 95 && $puntos->per <= 100):
											$color = "verde";
											$percentage = round($total_ccc_d / $total_obj_d * 100, 1);
											break;
										case $puntos->per >= 100:
											$color = "verde";
											$percentage = round($total_ccc_d / $total_obj_d * 100, 1);
											break;
									}
								?>
								<tr>
									<td><?php if($distribuidor_prima != $puntos->distribuidor) { echo "<strong>". $puntos->distribuidor ."</strong>"; $distribuidor_prima = $puntos->distribuidor; } ?></td>
									<td><?php echo  $puntos->linea ?></td>
									<td><?php echo  $puntos->objetivo ?></td>
									<td><?php echo  $puntos->ccc ?></td>
									<td>
										<div class="barra">
											<?php if($percentage > 100) { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="100%" height="15" alt="100%"/>
											<?php } else { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="<?php echo $percentage ?>%" height="15" alt="100%"/>
											<?php } ?>
										</div> 
									</td>
									<td><?php echo  $percentage ?>%</td>
								</tr>
							<?php } ?>
							
							<tr>
								<td colspan="2">TOTAL GENERAL</td>
								<td><?php echo $acum_obj_d?></td>
								<td><?php echo $acum_ccc_d?></td>
								<td>
									<div class="barra">
										<img src="img/barra-verde.png" width="100%" height="15" alt="100%"/>
									</div> 
								</td>
								<td><?php echo round($acum_ccc_d / $acum_obj_d * 100, 1); ?>%</td>
							</tr>
						</tbody>
					</table>	
					
				</div>
			</div>

			<!--Objetivos POR VENDEDOR-->
			<div id="distribuidor_contenido"  class="row" style="padding-top:20px; display:none">
			<div class="col-lg-12">
					
					<table class="table table-sm">
						<thead class="thead-light">
							<tr>
								<th width="12%">Vendedor</th>
								<th width="28%">Producto</th>
								<th width="12%">Objetivo</th>
								<th width="12%">CCC</th>
								<th width="30%">Porcentaje</th>
								<th width="6%">%</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$total_obj_v = 0;
								$total_ccc_v = 0;

								$acum_obj_v = 0;
								$acum_ccc_v = 0;
							?>
							<?php foreach ($puntos_by_vendedor as $puntos) { ?>
								<?php 
									$total_obj_v = $puntos->objetivo;
									$total_ccc_v = $puntos->ccc;
									
									$acum_obj_v += $total_obj_v;
									$acum_ccc_v += $total_ccc_v;

									$color = "verde";
									$percentage = 0;

									switch ($puntos->per) {
										case $puntos->per < 75:
											$color = "rojo";
											$percentage = round($total_ccc_v / $total_obj_v * 100, 1);
											break;
										case ($puntos->per >= 75 && $puntos->per <= 95):
											$color = "amarillo";
											$percentage = round($total_ccc_v / $total_obj_v * 100, 1);
											break;
										case ($puntos->per > 95 && $puntos->per <= 100):
											$color = "verde";
											$percentage = round($total_ccc_v / $total_obj_v * 100, 1);
											break;
										case $puntos->per >= 100:
											$color = "verde";
											$percentage = round($total_ccc_v / $total_obj_v * 100, 1);
											break;
									}
								?>
								<tr>
									<td><?php if($vendedor_prima != $puntos->vendedor) { echo "<strong>". $puntos->vendedor ."</strong>"; $vendedor_prima = $puntos->vendedor; } ?></td>
									<td><?php echo  $puntos->linea ?></td>
									<td><?php echo  $puntos->objetivo ?></td>
									<td><?php echo  $puntos->ccc ?></td>
									<td>
										<div class="barra">
											<?php if($percentage > 100) { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="100%" height="15" alt="100%"/>
											<?php } else { ?>
												<img src="img/barra-<?php echo $color ?>.png" width="<?php echo $percentage ?>%" height="15" alt="100%"/>
											<?php } ?>
										</div> 
									</td>
									<td><?php echo  $percentage ?>%</td>
								</tr>
							<?php } ?>
							
							<tr>
								<td colspan="2">TOTAL GENERAL</td>
								<td><?php echo $acum_obj_v?></td>
								<td><?php echo $acum_ccc_v?></td>
								<td>
									<div class="barra">
										<img src="img/barra-verde.png" width="100%" height="15" alt="100%"/>
									</div> 
								</td>
								<td><?php echo round($acum_ccc_v / $acum_obj_v * 100, 1); ?>%</td>
							</tr>
						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</section>

<script>
	<?php if ($_SESSION['QLMSF_rango'] == "vendedor") {?>
		showDistribuidor();
		function showDistribuidor() {		
			document.getElementById('region_contenido').style.display='none';	
			document.getElementById('linea_contenido').style.display='none';	
			document.getElementById('distribuidor_contenido').style.display='flex';	

			document.getElementById('distribuidor_solapa').classList.add("active");	
			document.getElementById('linea_solapa').classList.remove("active");	
			document.getElementById('region_solapa').classList.remove("active");	
		}
	<?php } else if ($_SESSION['QLMSF_rango'] == "supervisor") {?>
		function showLinea() {	
			document.getElementById('distribuidor_contenido').style.display='none';	
			document.getElementById('region_contenido').style.display='none';	
			document.getElementById('linea_contenido').style.display='flex';	

			document.getElementById('linea_solapa').classList.add("active");
			document.getElementById('distribuidor_solapa').classList.remove("active");	
			document.getElementById('region_solapa').classList.remove("active");	
		}

		function showDistribuidor() {		
			document.getElementById('region_contenido').style.display='none';	
			document.getElementById('linea_contenido').style.display='none';	
			document.getElementById('distribuidor_contenido').style.display='flex';	

			document.getElementById('distribuidor_solapa').classList.add("active");	
			document.getElementById('linea_solapa').classList.remove("active");	
			document.getElementById('region_solapa').classList.remove("active");	
		}
	<?php } else {?>
		function showRegion() {
			document.getElementById('linea_contenido').style.display='none';	
			document.getElementById('distribuidor_contenido').style.display='none';	
			document.getElementById('region_contenido').style.display='flex';	

			document.getElementById('region_solapa').classList.add("active");
			document.getElementById('linea_solapa').classList.remove("active");	
			document.getElementById('distribuidor_solapa').classList.remove("active");	
		}

		function showLinea() {	
			document.getElementById('distribuidor_contenido').style.display='none';	
			document.getElementById('region_contenido').style.display='none';	
			document.getElementById('linea_contenido').style.display='flex';	

			document.getElementById('linea_solapa').classList.add("active");
			document.getElementById('distribuidor_solapa').classList.remove("active");	
			document.getElementById('region_solapa').classList.remove("active");	
		}

		function showDistribuidor() {		
			document.getElementById('region_contenido').style.display='none';	
			document.getElementById('linea_contenido').style.display='none';	
			document.getElementById('distribuidor_contenido').style.display='flex';	

			document.getElementById('distribuidor_solapa').classList.add("active");	
			document.getElementById('linea_solapa').classList.remove("active");	
			document.getElementById('region_solapa').classList.remove("active");	
		}
	<?php }?>
</script>