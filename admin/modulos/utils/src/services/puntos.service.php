<?php
    require_once("autoload.php");
    require_once("conexion.php");
    
    class PuntosService extends Conexion {
        private $conexion;

        public function __construct() {
            $this->conexion = new Conexion();
            $this->conexion = $this->conexion->connect();
        }

        public function getPuntos() {
            $handler = $this->conexion->query("SELECT * FROM puntos");
            
            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosBySocio($id) {
            $handler = $this->conexion->prepare("SELECT * FROM puntos WHERE id_socio = :id");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosGroupByRegion($id, $mes = '08') {
            $handler = $this->conexion->prepare("SELECT region, SUM(puntos) as puntos FROM puntos WHERE id_socio = :id and MONTH(fecha) = :mes GROUP BY region");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        /* INICIO RANKING POR KAS */
        // 1ra Consulta.
        public function getRankingPuntosKas1($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_kas,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_kas = socios.id)
                WHERE 
                    id_kas = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_kas
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 2do Consulta.
        public function getRankingPuntosKas2($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_kam,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_kam = socios.id)
                WHERE 
                    id_kas = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_kam
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 3ra Consulta.
        public function getRankingPuntosKas3($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_supervisor,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_supervisor = socios.id)
                WHERE 
                    id_kas = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_supervisor
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 4ta Consulta.
        public function getRankingPuntosKas4($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_socio,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_socio = socios.id)
                WHERE 
                    id_kas = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_socio
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        /* FIN RANKING POR KAS */

        /* INICIO RANKING POR KAM */
        // 1ra Consulta.
        public function getRankingPuntosKam1($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_kam,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_kam = socios.id)
                WHERE 
                    id_kam = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_kam
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 2da Consulta.
        public function getRankingPuntosKam2($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_supervisor,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_supervisor = socios.id)
                WHERE 
                    id_kam = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_supervisor
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 3ra Consulta.
        public function getRankingPuntosKam3($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_socio,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_socio = socios.id)
                WHERE 
                    id_kam = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_socio
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        /* FIN RANKING POR KAM */

        /* INICIO RANKING POR SUP */
        // 1ra Consulta.
        public function getRankingPuntosSup1($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_supervisor,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_supervisor = socios.id)
                WHERE 
                    id_supervisor = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_supervisor
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        // 2da Consulta.
        public function getRankingPuntosSup2($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_socio,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_socio = socios.id)
                WHERE 
                    id_supervisor = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_socio
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        /* FIN RANKING POR SUP */

        /* INICIO RANKING POR VEN */
        // 1ra Consulta.
        public function getRankingPuntosVen($id, $mes = '08') {
            $handler = $this->conexion->prepare("
                SELECT 
                    id_socio,
                    CONCAT(apellido, ', ', nombre) as nombre,
                    rango,
                    SUM(puntos) as puntos
                FROM 
                    puntos 
                JOIN 
                    socios on (puntos.id_socio = socios.id)
                WHERE 
                    id_socio = :id and
                    MONTH(fecha) = :mes
                GROUP BY
                    id_socio
                ORDER BY
                    puntos DESC
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->bindParam(':mes', $mes, PDO::PARAM_STR);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }
        /* FIN RANKING POR VEN */

        public function getPuntosGroupByRegionCampeonato() {
            $handler = $this->conexion->prepare("SELECT region, SUM(puntos) as puntos FROM puntos GROUP BY region");
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosGroupByMonth($id) {
            $handler = $this->conexion->prepare("SELECT fecha, SUM(puntos) as puntos FROM puntos WHERE id_socio = :id GROUP BY fecha");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosBalanceByUser($id) {
            $handler = $this->conexion->prepare("SELECT fecha, linea, SUM(puntos) as puntos FROM puntos WHERE id_socio = :id GROUP BY fecha, linea");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByRegion($id, $mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    concepto,
                    region,
                    linea,
                    ROUND(SUM(objetivo)) as objetivo,
                    ROUND(SUM(avance_ccc)) as ccc,
                    ROUND((SUM(avance_ccc) / SUM(objetivo)) * 100) as per
                from
                    puntos
                where
                    concepto = '" . $concepto . "' and
                    MONTH(fecha) = " . $mes . " and
                    id_socio = " . $id . "
                group by
                    region,
                    linea
                order by
                    region,
                    linea
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByRegionKam($mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    concepto,
                    region,
                    linea,
                    ROUND(SUM(objetivo)) as objetivo,
                    ROUND(SUM(avance_ccc)) as ccc,
                    ROUND((SUM(avance_ccc) / SUM(objetivo)) * 100) as per
                from
                    puntos
                where
                    concepto = '" . $concepto . "' and
                    MONTH(fecha) = " . $mes . " and
                    region <> 'bsas'
                group by
                    region,
                    linea
                order by
                    region,
                    linea
            ");
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByRegionKamBSAS($mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    concepto,
                    region,
                    linea,
                    ROUND(SUM(objetivo)) as objetivo,
                    ROUND(SUM(avance_ccc)) as ccc,
                    ROUND((SUM(avance_ccc) / SUM(objetivo)) * 100) as per
                from
                    puntos
                where
                    concepto = '" . $concepto . "' and
                    MONTH(fecha) = " . $mes . " and
                    region = 'bsas'
                group by
                    region,
                    linea
                order by
                    region,
                    linea
            ");
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByDistribuidor($id, $mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    p.concepto,
                    p.distri as distribuidor,
                    p.linea,
                    ROUND(SUM(p.objetivo)) as objetivo,
                    ROUND(SUM(p.avance_ccc)) as ccc,
                    ROUND((SUM(p.avance_ccc) / SUM(p.objetivo)) * 100) as per
                from
                    puntos as p
                join 
                    socios as s on (s.id = p.id_socio)
                where
                    p.concepto = '" . $concepto . "' and
                    MONTH(p.fecha) = " . $mes . " and
                    id_socio = " . $id . "
                group by
                    p.distri,
                    p.linea
                order by
                    p.distri,
                    p.linea
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByDistribuidorKam($mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    p.concepto,
                    p.distri as distribuidor,
                    p.linea,
                    p.region,
                    ROUND(SUM(p.objetivo)) as objetivo,
                    ROUND(SUM(p.avance_ccc)) as ccc,
                    ROUND((SUM(p.avance_ccc) / SUM(p.objetivo)) * 100) as per
                from
                    puntos as p
                join 
                    socios as s on (s.id = p.id_socio)
                where
                    p.concepto = '" . $concepto . "' and
                    MONTH(p.fecha) = " . $mes . " and
                    p.region <> 'bsas'
                group by
                    p.distri,
                    p.linea
                order by
                    p.distri,
                    p.linea
            ");
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByDistribuidorKamBSAS($mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    p.concepto,
                    p.distri as distribuidor,
                    p.linea,
                    ROUND(SUM(p.objetivo)) as objetivo,
                    ROUND(SUM(p.avance_ccc)) as ccc,
                    ROUND((SUM(p.avance_ccc) / SUM(p.objetivo)) * 100) as per
                from
                    puntos as p
                join 
                    socios as s on (s.id = p.id_socio)
                where
                    p.concepto = '" . $concepto . "' and
                    MONTH(p.fecha) = " . $mes . " and
                    p.region = 'bsas'
                group by
                    p.distri,
                    p.linea
                order by
                    p.distri,
                    p.linea
            ");
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getPuntosMonthBySocioGroupByVendedor($id, $mes, $concepto) {
            $handler = $this->conexion->prepare("
                select 
                    p.concepto,
                    s.apellido as vendedor,
                    p.linea,
                    ROUND(SUM(p.objetivo)) as objetivo,
                    ROUND(SUM(p.avance_ccc)) as ccc,
                    ROUND((SUM(p.avance_ccc) / SUM(p.objetivo)) * 100) as per
                from
                    puntos as p
                join 
                    socios as s on (s.id = p.id_socio)
                where
                    p.concepto = '" . $concepto . "' and
                    MONTH(p.fecha) = " . $mes . "
                group by
                    p.id_socio,
                    p.linea
                order by
                    p.id_socio,
                    p.linea
            ");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        // Resguardo
        // public function getPuntosMonthBySocioGroupByVendedor($id, $mes, $concepto) {
        //     $handler = $this->conexion->prepare("
        //         select 
        //             p.concepto,
        //             s.apellido as vendedor,
        //             p.linea,
        //             ROUND(SUM(p.objetivo)) as objetivo,
        //             ROUND(SUM(p.avance_ccc)) as ccc,
        //             ROUND((SUM(p.avance_ccc) / SUM(p.objetivo)) * 100) as per
        //         from
        //             puntos as p
        //         join 
        //             socios as s on (s.id = p.id_socio)
        //         where
        //             p.concepto = '" . $concepto . "' and
        //             MONTH(p.fecha) = " . $mes . " and
        //             id_socio = " . $id . "
        //         group by
        //             p.id_socio,
        //             p.linea
        //         order by
        //             p.id_socio,
        //             p.linea
        //     ");
        //     $handler->bindParam(':id', $id, PDO::PARAM_INT);
        //     $handler->execute();

        //     return $handler->fetchAll(PDO::FETCH_OBJ);
        // }
    }
?>