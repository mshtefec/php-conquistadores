

var img1 = document.querySelector('.frm_alta_imagen1');
var img2 = document.querySelector('.frm_alta_imagen2');
var img3 = document.querySelector('.frm_alta_imagen3');
var img4 = document.querySelector('.frm_alta_imagen4');
var img5 = document.querySelector('.frm_alta_imagen5');


img2.disabled = true;
img3.disabled = true;
img4.disabled = true;
img5.disabled = true;

img1.addEventListener('change' , () => {
        if (img1.value == "") {
            img2.disabled = true;
            img2.value = "";
            img3.disabled = true;
            img3.value = "";
            img4.disabled = true;
            img4.value = "";
            img5.disabled = true;
            img5.value = "";
        } else {
            img2.disabled = false;
        }    
});

img2.addEventListener('change' , () => {
    if (img2.value  == "") {
        img3.disabled = true;
        img3.value = "";
        img4.disabled = true;
        img4.value = "";
        img5.disabled = true;
        img5.value = "";
    } else {
        img3.disabled = false;
    }
});

img3.addEventListener('change' , () => {
    if (img3.value  == "") {
        img4.disabled = true;
        img4.value = "";
        img5.disabled = true;
        img5.value = "";
    } else {
        img4.disabled = false;
    }
});

img4.addEventListener('change' , () => {
    if (img4.value  == "") {
        img5.disabled = true;
        img5.value = "";
    } else {
        img5.disabled = false;
    }
});

