<?php 

    session_start();

    include('scripts/db.php');

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $anio = $_POST['anio'];
        $mes = $_POST['mes'];
        $concepto = $_POST['concepto'];
        $region = $_POST['region'];
        $distri = $_POST['distri'];
        $id_socio = $_POST['id_socio'];
        $producto = $_POST['producto'];
        $objetivo = $_POST['objetivo'];
        $avance_ccc = $_POST['avance_ccc'];
        $avance_per = $_POST['avance_per'];
        $id_sup = $_POST['id_sup'];
        $id_kam = $_POST['id_kam'];
        $id_kas = $_POST['id_kas'];
        $peso = $_POST['peso'];
        $puntos = $_POST['puntos'];
        $ranking_mes = $_POST['ranking_mes'];
        $ranking_acum = $_POST['ranking_acum'];

        $stmt = $dbh->prepare("
            INSERT INTO puntos (
                anio,
                mes,
                concepto,
                region,
                distri,
                id_socio,
                producto,
                objetivo,
                avance_ccc,
                avance_per,
                id_sup,
                id_kam,
                id_kas,
                peso,
                puntos,
                ranking_mes,
                ranking_acum
            ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        ");

        $stmt->execute([
            $anio,
            $mes,
            $concepto,
            $region,
            $distri,
            $id_socio,
            $producto,
            $objetivo,
            $avance_ccc,
            $avance_per,
            $id_sup,
            $id_kam,
            $id_kas,
            $peso,
            $puntos,
            $ranking_mes,
            $ranking_acum
        ]);

        header("Location: index.php");
    }

?>


<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Nuevo de Punto
                    <a type="button" class="btn btn-primary" href="index.php"><i class="bi bi-arrow-return-left"></i> Volver a Puntos</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Nuevo Punto</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Nuevo de Punto</h6>
                    
                    <form action="new.php" method="post">
                        <hr class="my-4">

                        <div class="row g-3">
                            <div class="col-md-4">
                                <label for="anio" class="form-label">Año</label>
                                <input type="text" class="form-control" id="anio" name="anio">
                            </div>
                            <div class="col-md-4">
                                <label for="mes" class="form-label">Mes</label>
                                <input type="text" class="form-control" id="mes" name="mes">
                            </div>
                            <div class="col-md-4">
                                <label for="concepto" class="form-label">Concepto</label>
                                <input type="text" class="form-control" id="concepto" name="concepto">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="region" class="form-label">Region</label>
                                <input type="text" class="form-control" id="region" name="region">
                            </div>
                            <div class="col-md-3">
                                <label for="distri" class="form-label">Distri</label>
                                <input type="text" class="form-control" id="distri" name="distri">
                            </div>
                            <div class="col-md-3">
                                <label for="id_socio" class="form-label">ID Socio</label>
                                <input type="text" class="form-control" id="id_socio" name="id_socio">
                            </div>
                            <div class="col-md-3">
                                <label for="producto" class="form-label">Producto</label>
                                <input type="text" class="form-control" id="producto" name="producto">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="objetivo" class="form-label">Objetivo</label>
                                <input type="text" class="form-control" id="objetivo" name="objetivo">
                            </div>
                            <div class="col-md-3">
                                <label for="avance_ccc" class="form-label">Avance CCC</label>
                                <input type="text" class="form-control" id="avance_ccc" name="avance_ccc">
                            </div>
                            <div class="col-md-3">
                                <label for="avance_per" class="form-label">Avance %</label>
                                <input type="text" class="form-control" id="avance_per" name="avance_per">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="id_sup" class="form-label">ID Supervisor</label>
                                <input type="text" class="form-control" id="id_sup" name="id_sup">
                            </div>
                            <div class="col-md-3">
                                <label for="id_kam" class="form-label">ID Kam</label>
                                <input type="text" class="form-control" id="id_kam" name="id_kam">
                            </div>
                            <div class="col-md-3">
                                <label for="id_kas" class="form-label">ID Kas</label>
                                <input type="text" class="form-control" id="id_kas" name="id_kas">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-4">
                                <label for="peso" class="form-label">Peso</label>
                                <input type="text" class="form-control" id="peso" name="peso">
                            </div>
                            <div class="col-md-4">
                                <label for="ranking_mes" class="form-label">Ranking Mes</label>
                                <input type="text" class="form-control" id="ranking_mes" name="ranking_mes">
                            </div>
                            <div class="col-md-4">
                                <label for="ranking_acum" class="form-label">Ranking Acum</label>
                                <input type="text" class="form-control" id="ranking_acum" name="ranking_acum">
                            </div>
                        </div>

                        <hr class="my-4">

                        <button class="w-100 btn btn-success btn" type="submit">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>