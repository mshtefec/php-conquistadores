<?php 
    include_once("../../php/class/configuracion.php");

    session_start();

    $host = DDBBHOST;
    $dbname = DDBBDATABASE;
    $user = DDBBUSER;
    $pass = DDBBPASSWORD;
    $locale = 'es_AR';
    
    $dbh = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8', $user, $pass, [
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET lc_time_names='" . $locale . "'" 
    ]);

?>