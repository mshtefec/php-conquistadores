<?php 

    include('db.php');

    $ganadores = array();

    $stmt = $dbh->prepare("
        SELECT 
            g.idsocio as id,
            CONCAT(s.apellido, ', ', s.nombre) as nombre,
            s.region as region,
            s.rango as rango,
            DATE_FORMAT(g.fecha, '%M') as mes, 
            YEAR(g.fecha) as 'año'
        FROM 
            ganadores as g
        JOIN
            socios as s ON (g.idsocio = s.idsocio)
    ");
    $stmt->execute();
    $ganadores = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `ganadores` WHERE `idsocio`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>