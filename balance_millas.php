<?php
require_once 'admin/php/class/class.socio.php';

if(!isset($_SESSION)) {session_start();}
$idcliente = $_SESSION['QLMSF_id_socio'];

$clnt = new socio();
if($clnt->select($idcliente)){
	$puntos_totales = $clnt->dameSumaXConcepto('puntos','puntos','','');

	$saldo = $puntos_totales;

	$_SESSION['QLMSF_puntos'] = $saldo;
}

	include('admin/modulos/utils/src/services/puntos.service.php');

	if(isset($_SESSION['socio'])) {
		$id = $_SESSION['socio'];
		
		$puntosSrv = new PuntosService();
		
		$puntos_by_month = $puntosSrv->getPuntosBalanceByUser($id);
	}

?>
<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v4.0.1">
		<title>Conquistadores</title>
		<!--link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/"-->
		<!-- Bootstrap core CSS -->
		<link href="assets/dist/css/bootstrap.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="css/peniaflor.css" rel="stylesheet">
	</head>
	<body>
		<?php include_once('cabecera.php');?>
		<main role="main" style="margin-bottom: 4.3rem;">
			
			<section class="gp-balance">
				<div class="container balance">
					<h2>Mi Balance</h2>
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-10">
							<h3><small>Saldo al <?php echo date("j/n");?>:</small> <?php echo $saldo;?> puntos</h3>
							<table class="table table-sm">
								<thead class="thead-light">
									<tr>
										<th width="12%"><strong>Fecha</strong></th>
										<th width="73%"><strong>Detalle</strong></th>
										<th width="15%" style="text-align:center"><strong>Puntos</strong></th>
									</tr>
								</thead>
								<tbody>
									<?php $mes_linea; ?>
									<?php foreach ($puntos_by_month as $key => $puntos) { ?>
										<tr>
											<?php 
												if ($key == 0) {
													$mes_linea = $puntos->fecha;
													?>
														<td><?php echo date("d/m/Y", strtotime($mes_linea));?></td>
													<?php 
												} else {
													$fecha_linea = $puntos->fecha;
													$fecha_mes = $mes_linea;
													if ($fecha_linea != $fecha_mes) {
														$mes_linea = $fecha_linea;
														?>
															<td><?php echo date("d/m/Y", strtotime($mes_linea));?></td>
														<?php
													} else {
														?>
															<td></td>
														<?php 
													}
												}
											?>
											<td><?php echo $puntos->linea;?></td>
											<td style="text-align:center"><?php echo $puntos->puntos;?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="col-lg-1"></div>
					</div>
					<!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
		<script src="assets/dist/js/bootstrap.bundle.js"></script>

		<?php include_once('pie.php');?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171840062-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-171840062-1');
		</script>
	</body>
</html>
