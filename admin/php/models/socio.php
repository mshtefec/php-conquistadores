<?php

class Socio
{
    private $idsocio;
    private $idtipo;
    private $codigo;
    private $region;
    private $rango;
    private $ejecutivo;
    private $jefe;			
    private $gerente;		
    private $nombre;   	
    private $apellido;  
    private $email;   	
    private $clave;   	
    private $direccion; 
    private $localidad; 
    private $provincia; 
    private $telefono;  
    private $fechaalta; 
    private $lastlog;

    function getIdsocio()
    {
        return $this->idsocio;
    }

    function setIdsocio($idsocio) 	
    {
        $this->idsocio = $idsocio;
    }

    function getIdtipo() 		
    {
        return $this->idtipo;
    }

    function setIdtipo($idtipo) 		
    {
        $this->idtipo = $idtipo;
    }

    function getCodigo() 		
    {
        return $this->codigo;
    }

    function setCodigo($codigo) 		
    {
        $this->codigo = $codigo;
    }

    function getRegion() 		
    {
        return $this->region;
    }

    function setRegion($region) 		
    {
        $this->region = $region;
    }

    function getRango() 		
    {
        return $this->rango;
    }

    function setRango($rango) 		
    {
        $this->rango = $rango;
    }

    function getEjecutivo() 
    {
        return $this->ejecutivo;
    }

    function setEjecutivo($ejecutivo) 
    {
        $this->ejecutivo = $ejecutivo;
    }

    function getJefe() 			
    {
        return $this->jefe;
    }

    function setJefe($jefe) 			
    {
        $this->jefe = $jefe;
    }

    function getGerente() 	
    {
        return $this->gerente;
    }

    function setGerente($gerente) 	
    {
        $this->gerente = $gerente;
    }

    function getNombre() 		
    {
        return $this->nombre;
    }

    function setNombre($nombre) 		
    {
        $this->nombre = $nombre;
    }

    function getApellido() 	
    {
        return $this->apellido;
    }

    function setApellido($apellido) 	
    {
        $this->apellido = $apellido;
    }

    function getEmail() 		
    {
        return $this->email;
    }

    function setEmail($email) 		
    {
        $this->email = $email;
    }

    function getClave() 		
    {
        return $this->clave;
    }

    function setClave($clave) 		
    {
        $this->clave = $clave;
    }

    function getDireccion()	
    {
        return $this->direccion;
    }

    function setDireccion($direccion) 
    {
        $this->direccion = $direccion;
    }

    function getLocalidad()	
    {
        return $this->localidad;
    }

    function setLocalidad($localidad) 
    {
        $this->localidad = $localidad;
    }

    function getProvincia()	
    {
        return $this->provincia;
    }

    function setProvincia($provincia) 
    {
        $this->provincia = $provincia;
    }

    function getTelefono() 	
    {
        return $this->telefono;
    }

    function setTelefono($telefono) 	
    {
        $this->telefono = $telefono;
    }

    function getFechaalta()	
    {
        return $this->fechaalta;
    }

    function setFechaalta($fechaalta)	
    {
        $this->fechaalta = $fechaalta;
    }

    function getLastlog()	
    {
        return $this->lastlog;
    }

    function setLastlog($lastlog)	
    {
        $this->lastlog = $lastlog;
    }

}