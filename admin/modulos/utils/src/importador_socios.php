<?php

    if (isset($_POST["import"])) {
        
        $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        
        if(in_array($_FILES["file"]["type"], $allowedFileType)) {

            $targetPath = __DIR__ . '\\temp\\' . date('d-m-Y_H_i_s') . '_' . $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

            if ( $xlsx = SimpleXLSX::parse( $targetPath ) ) {

                $sql = "INSERT INTO socios (
                    id, 
                    nombre, 
                    apellido, 
                    rango, 
                    clave, 
                    region, 
                    distribuidora, 
                    email, 
                    direccion, 
                    localidad, 
                    provincia, 
                    telefono,
                    fecha_carga
                ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

                $stmt = $conn->prepare($sql);
                
                $stmt->bindParam( 1, $id);
                $stmt->bindParam( 2, $nombre);
                $stmt->bindParam( 3, $apellido);
                $stmt->bindParam( 4, $rango);
                $stmt->bindParam( 5, $clave);
                $stmt->bindParam( 6, $region);
                $stmt->bindParam( 7, $distribuidora);
                $stmt->bindParam( 8, $email);
                $stmt->bindParam( 9, $direccion);
                $stmt->bindParam( 10, $localidad);
                $stmt->bindParam( 11, $provincia);
                $stmt->bindParam( 12, $telefono);
                $stmt->bindParam( 13, $fecha_carga);

                // voy a quitar la primera fila que son las cabeceras.
                $rows = $xlsx->rows();
                unset($rows[0]);
                
                foreach ( $rows as $key => $fields ) {

                    $id = (int) $fields[0];
                    $nombre = $fields[1];
                    $apellido = $fields[2];
                    $rango = $fields[3];
                    $clave = $fields[4];
                    $region = $fields[5];
                    $distribuidora = $fields[6];
                    $email = $fields[7];
                    $direccion = $fields[8];
                    $localidad = $fields[9];
                    $provincia = $fields[10];
                    $telefono = $fields[11];
                    $fecha_carga = date_create('now')->format('Y-m-d H:i:s');
                    
                    try {

                        $log_file .= 'Procesando la linea '. $key . PHP_EOL;

                        try {
                            $stmt->execute();
                        } catch (\Exception $e) {
                            echo "<p style='color: red;'>El ID_SOCIO (clave) " . $id . " ya se encuentra cargado en la base de datos de Socios.</p><br>";
                            echo "<p style='color: white;'>No se pueden cargar campos claves repetidos.</p><br>";
                        }

                        $type = "success";
                        $message = "Excel Importado Correctamente.";
                    } catch (Exception $e) {
                        echo $e;
                    }
                }

                file_put_contents('logs/log_importador_' . date("Y-m-d") . '.txt', $log_file, FILE_APPEND);
                
            } else {
                echo SimpleXLSX::parseError();
            }
            
        } else { 
            $type = "error";
            $message = "Suba un tipo de archivo invalido Excel.";
        }
    }
    
?>