﻿<?php

	set_time_limit(500);

	require_once '../../php/class/class.socio.php';
	require_once '../../php/generales.php';

	$hoy 	= date('d/m/Y');
	$d 		= date('d');
	$nd 	= date('N');
	$m 		= date('m');
	$m 		= $m + 0;
	$anio	= date('Y');

	//declaracion de variables
	$arr_materiales_excel = array();
	$tamanio = count($arr_materiales_excel);
	$situacion = '1';

	if(isset($_POST['proceso']) && $_POST['proceso']=='S') {
		if(isset($_FILES['frm_alta_csv']) && $_FILES['frm_alta_csv']['tmp_name']!='') {
			$fname = date('d-m-Y_H.i.s').'_'.$_FILES['frm_alta_csv']['name'];
			move_uploaded_file($_FILES['frm_alta_csv']['tmp_name'], 'csv/'.$fname);
			$row 	= 0;

			$file = fopen('csv/'.$fname, 'r') or exit('No se pudo abrir el archivo!');
			$nombre_archivo = $fname;

			//salteo la primer fila.
			$data = fgets($file);
			
			$indice_anterior = -1;
			$log_acciones = '';
			$log_file = '';
			$clnt = new socio();

			while(!feof($file)){
				$data = fgets($file);
				if(strlen($data)>20){
					$row++;
					$indice = $row;
					//debo hacer un split
					$_arr_content = preg_split('/;/', $data);
					if(count($_arr_content)>=10) {
						
						// OBLIGATORIOS
						$COD_SOCIO			= sanear_string(trim($_arr_content[0]));
						$NOMBRE				= sanear_string(trim($_arr_content[1]));
						$APELLIDO			= sanear_string(trim($_arr_content[2]));
						$RANGO				= sanear_string(trim($_arr_content[3]));
						$CLAVE				= sanear_string(trim($_arr_content[4]));
						$REGION				= sanear_string(trim($_arr_content[5]));
						$DISTRIBUIDORA		= sanear_string(trim($_arr_content[6]));
						
						// OPCIONALES
						$EMAIL				= sanear_string(trim($_arr_content[7]));
						$DIRECCION			= sanear_string(trim($_arr_content[8]));
						$LOCALIDAD 			= sanear_string(trim($_arr_content[9]));
						$PROVINCIA			= sanear_string(trim($_arr_content[10]));
						$TELEFONO			= sanear_string(trim($_arr_content[11]));

						$adv = '';
						if($COD_SOCIO == '') $adv.= '<font color="RED"> | FALTA EL CAMPO CODIGO DE SOCIO Y ES OBLIGATORIO </font>';
						if($NOMBRE == '') $adv.= '<font color="RED"> | FALTA EL CAMPO NOMBRE Y ES OBLIGATORIO </font>';
						if($APELLIDO == '') $adv.= '<font color="RED"> | FALTA EL CAMPO APELLIDO Y ES OBLIGATORIO </font>';
						if($RANGO == '') $adv.= '<font color="RED"> | FALTA EL CAMPO RANGO Y ES OBLIGATORIO </font>';
						if($CLAVE == '') $adv.= '<font color="RED"> | FALTA EL CAMPO CLAVE Y ES OBLIGATORIO </font>';
						if($REGION == '') $adv.= '<font color="RED"> | FALTA EL CAMPO REGION Y ES OBLIGATORIO </font>';
						if($DISTRIBUIDORA == '') $adv.= '<font color="RED"> | FALTA EL CAMPO DISTRIBUIDORA Y ES OBLIGATORIO </font>';

						// BUSCA SI EXISTE EL SOCIO, SI LO ENCUENTRA ACTUALIZA LOS DATOS SINO CREO UNO NUEVO
						if($clnt->select_X_codigo($COD_SOCIO)) {

							// OBLIGATORIOS
							$clnt->setCodigo($COD_SOCIO);
							$clnt->setNombre($NOMBRE);
							$clnt->setApellido($APELLIDO);
							$clnt->setRango($RANGO);
							$clnt->setClave($CLAVE);
							$clnt->setRegion($REGION);
							$clnt->setDistribuidora($DISTRIBUIDORA);

							// OPCIONALES
							$clnt->setEmail($EMAIL);
							$clnt->setDireccion($DIRECCION);
							$clnt->setLocalidad($LOCALIDAD);
							$clnt->setProvincia($PROVINCIA);
							$clnt->setTelefono($TELEFONO);

							var_dump($clnt);
							die();

							if($clnt->update($clnt->getIdcliente())) {	
								$log_acciones .= '<strong><font color="GREEN">- FILA '.$indice.' REEMPLAZADA - CODIGO UNICO '.$CODIGO_UNICO.' PREEXISTENTE EN LA BASE DE DATOS - DATOS ACTUALIZADOS '.$adv.'</strong></font><br />';
								$log_file .= '- FILA '.$indice.' REEMPLAZADA - CODIGO UNICO '.$CODIGO_UNICO.' PREEXISTENTE EN LA BASE DE DATOS - DATOS ACTUALIZADOS '.$adv.PHP_EOL;
							}

						} else { 

							// OBLIGATORIOS
							$clnt->setCodigo($COD_SOCIO);
							$clnt->setNombre($NOMBRE);
							$clnt->setApellido($APELLIDO);
							$clnt->setRango($RANGO);
							$clnt->setClave($CLAVE);
							$clnt->setRegion($REGION);
							$clnt->setDistribuidora($DISTRIBUIDORA);

							// OPCIONALES
							$clnt->setEmail($EMAIL);
							$clnt->setDireccion($DIRECCION);
							$clnt->setLocalidad($LOCALIDAD);
							$clnt->setProvincia($PROVINCIA);
							$clnt->setTelefono($TELEFONO);

							if($clnt->insert()) {
								$log_acciones .= '<strong><font color="GREEN">- FILA '.$indice.' INSERTADO CLIENTE: '.$RAZON_SOCIAL.' CUIT '.$CUIT.' '.$adv.'</strong></font><br />';
								$log_file .= '- FILA '.$indice.' INSERTADO CLIENTE: '.$RAZON_SOCIAL.' CUIT '.$CUIT.' '.$adv.PHP_EOL;
							} else {
								$log_acciones .= '<strong><font color="RED">- FILA '.$indice.' ERROR SQL AL INSERTAR ESTA LINEA</strong></font><br />';
								$log_file .= '- FILA '.$indice.' ERROR SQL AL INSERTAR ESTA LINEA'.PHP_EOL;
							}
							
						}					
							
					}
				}
			}
			file_put_contents('logs/log_'.date('Y-m-d').'.txt', $log_file, FILE_APPEND);
			$situacion = '3';		
			fclose($file);
		}
		$tamanio = count($arr_materiales_excel);	
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Conquistadores</title>

	<link href="../../css/estructura.css" rel="stylesheet" type="text/css" />
	<link href="../../css/listados.css" rel="stylesheet" type="text/css" />
	<link href="../../css/datepicker.css" rel="stylesheet" type="text/css" />
	<link href="../../css/forms.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../../css/estilos-2021.css" type="text/css">
</head>
<body>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td height="100" align="center" valign="middle">
       			<img style="width: 120px;" class="imagen-logo" src="../../../img/logo-05.png" alt="Conquistadores 2021"/>
			</td>
		</tr>
        <tr>
			<td>
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td valign="top">
							<table width="99%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="15"></td>
									<td width="100%" class="backTablaSup">&#160;</td>
									<td align="left"></td>
								</tr>
								<tr>
									<td class="backTablaIzq">&#160;</td>
									<td height="150" valign="top" bgcolor="#FFFFFF">
										<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
											<tr>
												<td height="25" valign="bottom" class="textoMarron14sinhover"><strong>IMPORTAR ARCHIVO DE SOCIOS</strong></td>
												<td height="25" valign="bottom" align="right" class="textNegro14"><!--<a href="#" class="textoMarron12sinhover" onclick="parent.cerrarframe();"><span class="textoMarron12sinhover"><strong>[X]</strong></span></a>--></td>
										  </tr>
											<tr>
												<td colspan="2" height="20" valign="bottom" class="textCeleste12"></td>
											</tr>
											<tr>
												<td colspan="2" height="450" valign="top">
                          							<div style="float:left">
														<?php if($situacion == '1'){?>
													<!-- MUESTRO EL FORM -->
														<form id="frm_alta" name="frm_alta" method="POST" enctype='multipart/form-data'>
															<input type="hidden" id="proceso" name="proceso" value="S">
															<table width="320" border="0" cellpadding="0" cellspacing="0">
																<tr>
																	<td height="55" class="textAzul12" style="border-top:solid 1px #999999">
																	 <strong>seleccione el archivo (.csv)</strong><BR>
																	 <input name="frm_alta_csv" type="file" class="inputImport" id="frm_alta_csv" size="40">
																	</td>
																</tr>
																<tr>
																	<td height="48" align="center" bgcolor="#e30613" style="border-top:solid 1px #999999">
																		<input name="procesar" type="submit" class="boton" id="procesar" value="Importar" />
																	</td>
																</tr>
															</table>
														</form>
                          							</div>
                          <div class="ayuda">
                          	<table>
															<tr>
																<td>&#160;&#160;&#160;</td>
																<td>&#160;&#160;&#160;</td>
																<td>Desde este modulo se procesa el archivo de socios. <strong>La primer fila del archivo sera ignorada dado que se espera contenga los nombres de cada columna.</strong><br /><br />El mismo debe contener la informaci&#243;n de los socios bajo una estructura determinada. Cada l&#237;nea deber&#225; tener los siguientes campos siempre separados por <strong>punto y coma</strong>.<br /><br /> 
																<span style="display:block; border:solid 1px #333; padding:15px">
																	<strong>FORMATO:</strong> COD_SOCIO; NOMBRE; APELLIDO; RANGO; CONTRASEÑA; REGION; DISTRIBUIDORA; EMAIL (OPC); DIRECCION (OPC); LOCALIDAD (OPC); PROVINCIA (OPC); TELÉFONO (OPC);
																</span></td>
															</tr>
														</table>
                          </div>
													<?php }?>

													<?php if($situacion == '2'){?>
													<form name="formdatos" id="formdatos" action="importacion_sups.php" method="POST">
														<table width="100%" border="1" cellpadding="5" cellspacing="5" style="border: 1px solid black; font-size:12px; border-collapse: collapse;">
															<tr>
																<td><span></span></td>
																<td><span>CODIGO SOCIO</span></td>
																<td><span>NOMBRE</span></td>
																<td><span>APELLIDO</span></td>
																<td><span>CLAVE</span></td>
																<td><span>EMAIL</span></td>
																<td><span>DOMICILIO</span></td>
																<td><span>LOCALIDAD</span></td>
																<td><span>PROVINCIA</span></td>
																<td><span>TELEFONO</span></td>
															</tr>
															<!-- LA PANTALLA DE PREVIEW DE CARGA-->
															<?php
															for($i=0;$i<$tamanio;$i++)
															{
															?>
															<tr>
																<td>
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['CODIGO_SOCIO'];?>" name="<?php echo $i?>-CODIGO_SOCIO">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['NOMBRE'];?>" name="<?php echo $i?>-NOMBRE">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['APELLIDO'];?>" name="<?php echo $i?>-APELLIDO">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['CLAVE'];?>" name="<?php echo $i?>-CLAVE">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['EMAIL'];?>" name="<?php echo $i?>-EMAIL">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['DOMICILIO'];?>" name="<?php echo $i?>-DOMICILIO">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['DOMICILIO_LOCALIDAD'];?>" name="<?php echo $i?>-DOMICILIO_LOCALIDAD">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['DOMICILIO_PROVINCIA'];?>" name="<?php echo $i?>-DOMICILIO_PROVINCIA">
																	<input type="hidden" value="<?php echo $arr_materiales_excel[$i]['TELEFONO'];?>" name="<?php echo $i?>-TELEFONO">
																	<input type="checkbox" id="" value="S" name="<?php echo $i?>-chequeado" checked>
																	<input type="hidden" value="<?php echo $nombre_archivo;?>" name="nombre_archivo">
																</td>
																<td><?php echo $arr_materiales_excel[$i]['CODIGO_SOCIO'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['NOMBRE'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['APELLIDO'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['CLAVE'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['EMAIL'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['DOMICILIO'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['DOMICILIO_LOCALIDAD'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['DOMICILIO_PROVINCIA'];?></td>
																<td><?php echo $arr_materiales_excel[$i]['TELEFONO'];?></td>
															</tr>
															<?php
															}
															?>
														</table>
														<table width="830" border="0" cellpadding="0" cellspacing="0">
															<tr>
																<td width="220" height="35" align="left" valign="bottom">
																	<input type="submit" name="enviar" value="IMPORTAR" class="boton">
																</td>
															</tr>
														</table>
													</form>
													<?php }?>

													<?php if($situacion == '3'){?>
													<!-- LA PANTALLA DE LOS DATOS INSERTADOS-->
													<span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><?php echo $log_acciones;?></span>
													<?php }?>
                        </td>
										  </tr>
										</table>
									</td>
									<td class="backTablaDer">&#160;</td>
								</tr>
								<tr>
									<td></td>
									<td class="backTablaPie">&#160;</td>
									<td></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>&#160;</td>
		</tr>
	</table>
</body>
</html>