﻿<?php
	//includes
	set_time_limit(500);
	ini_set('memory_limit', '1024M');
	ini_set('post_max_size', '200M');

	require_once '../../php/class/class.ganador.php';
	require_once '../../php/generales.php';

	//declaracion de variables
	$arr_materiales_excel = array();
	$tamanio = count($arr_materiales_excel);
	$situacion = '1';

	if(isset($_POST['proceso']) && $_POST['proceso']=='S') {

		if(isset($_FILES['frm_alta_csv']) && $_FILES['frm_alta_csv']['tmp_name']!='')	{

			$fname = date('d-m-Y_H.i.s').'_'.$_FILES['frm_alta_csv']['name'];
			move_uploaded_file($_FILES['frm_alta_csv']['tmp_name'], 'csv/' . $fname);
			$row = 0;

			$file = fopen('csv/'. $fname, 'r') or exit('No se pudo abrir el archivo!');
			$nombre_archivo = $fname;
			
			$data = fgets($file);

			$gdor = new ganador();
			
			$log_acciones = '';
			$log_file  = '';

			while(!feof($file)){

				$data = fgets($file);
				$row++;
				//debo hacer un split
				$_arr_content = preg_split('/;/', $data);

				if( count($_arr_content)>=6 ) {

					$IDSOCIO			=	sanear_string(trim($_arr_content[0]));
					$ANIO				=	sanear_string(trim($_arr_content[1]));
					$MES				=	sanear_string(trim($_arr_content[2]));
					$CONCEPTO			=	sanear_string(trim($_arr_content[3]));
					$RANKING			=	trim($_arr_content[4]);
					$PUNTAJE			=	trim($_arr_content[5]);

					$indice = $row;
					
					//armo el array completo
					$arr_materiales_excel[$row-1]['IDSOCIO']	= $IDSOCIO;
					$arr_materiales_excel[$row-1]['ANIO']		= $ANIO;
					$arr_materiales_excel[$row-1]['MES']		= $MES;
					$arr_materiales_excel[$row-1]['CONCEPTO']	= $CONCEPTO;
					$arr_materiales_excel[$row-1]['RANKING']	= $RANKING;
					$arr_materiales_excel[$row-1]['PUNTAJE']	= $PUNTAJE;

					$log_acciones .= '<br /><div class="impTt">Procesando la linea '. $indice .'</div>';
					$log_file .= 'Procesando la linea '. $indice . PHP_EOL;

					$adv = '';
					
					$FECHA = $ANIO . '-' . $MES . '-01';

					$gdor->setIdsocio($IDSOCIO);
					$gdor->setFecha($FECHA);
					$gdor->setConcepto($CONCEPTO);
					$gdor->setRanking($RANKING);
					$gdor->setPuntaje($PUNTAJE);

					if($gdor->insert($IDSOCIO)){
						$log_acciones .= '<strong><font color="GREEN">- FILA ' . $indice . ' SE CARGO CORRECTAMENTE' . $adv . '</strong></font><br />';
						$log_file .= '- FILA ' . $indice . ' SE CARGO CORRECTAMENTE' . $adv . PHP_EOL;
					} else {
						$log_acciones .= '<strong><font color="RED">- FILA ' . $indice . ' ERROR SQL AL INSERTAR ESTA LINEA</strong></font><br />';
						$log_file .= '- FILA ' . $indice . ' ERROR SQL AL INSERTAR ESTA LINEA' . PHP_EOL;
					}

				}
			}

			fclose($file);
		}

		file_put_contents('logs/log_millas_' . date("Y-m-d") . '.txt', $log_file, FILE_APPEND);
		$situacion = '3';
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Conquistadores</title>
		<style type="text/css">
			body {
				background-color: #1d1d1d;
				margin-left: 0px;
				margin-top: 0px;
				margin-right: 0px;
				margin-bottom: 0px;
				font-family: Arial, Helvetica, sans-serif;
			}
		</style>
	</head>
	<body>
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td height="100" align="center" valign="middle">
					<img width="120" class="imagen-logo" src="../../../img/logo-05.png" alt="Conquistadores 2021"/>
				</td>
			</tr>
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td valign="top">
								<table width="99%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="15"></td>
										<td width="100%" class="backTablaSup">&#160;</td>
										<td align="left"></td>
									</tr>
									<tr>
										<td class="backTablaIzq">&#160;</td>
										<td height="150" valign="top" bgcolor="#FFFFFF">
											<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
												<tr>
													<td height="25" valign="bottom" class="textoMarron14sinhover"><strong>AGREGAR GANADORES</strong></td>
													<td height="25" valign="bottom" align="right" class="textNegro14"><a href="#" class="textoMarron12sinhover" onclick="parent.cerrarframe();"><span class="textoMarron12sinhover"><strong>[X]</strong></span></a></td>
												</tr>
												<tr>
													<td colspan="2" height="20" valign="bottom" class="textCeleste12"></td>
												</tr>
												<tr>
													<td colspan="2" height="450" valign="top">
														<div style="float:left">
															<?php if($situacion == '1'){?>
															<!-- MUESTRO EL FORM -->
															<form id="frm_alta" name="frm_alta" method="POST" enctype='multipart/form-data'>
																<input type="hidden" id="proceso" name="proceso" value="S">
																<table width="320" border="0" cellpadding="0" cellspacing="0">
																	<tr>
																		<td height="55" class="textAzul12" style="border-top:solid 1px #999999">
																			<strong>seleccione el archivo (.csv)</strong><br />
																			<input name="frm_alta_csv" type="file" class="inputImport" id="frm_alta_csv" size="40">
																		</td>
																	</tr>
																	<tr>
																		<td height="48" align="center" bgcolor="#e30613" style="border-top:solid 1px #999999">
																			<input name="procesar" type="submit" class="boton" id="procesar" value="Importar" />
																		</td>
																	</tr>
																</table>
															</form>
														</div>
														<div class="ayuda">
															<table>
																<tr>
																	<td>&#160;&#160;&#160;</td>
																	<td>&#160;&#160;&#160;</td>
																	<td>
																		Desde este modulo se procesa el archivo de ganadores para los socios adheridos.<strong>La primer fila del archivo sera ignorada dado que se espera contenga los nombres de cada columna.</strong><br /><br />El mismo debe contener la informaci&#243;n de los ganadores una estructura determinada. Cada l&#237;nea deber&#225; tener los siguientes campos siempre separados por <strong>punto y coma</strong>.<br /><br /><span style="display:block; border:solid 1px #333; padding:15px">
																		<strong>FORMATO PARA GANADORES:</strong><br>IDSOCIO; ANIO; MES; CONCEPTO; RANKING; PUNTOS;</span>
																	</td>
																</tr>
															</table>
														</div>
														<?php }?>

														<?php if($situacion == '3'){?>
														<!-- LA PANTALLA DE LOS DATOS INSERTADOS-->
														<span style="font-family:Arial, Helvetica, sans-serif; font-size:12px;"><?php echo $log_acciones;?></span>
														<?php }?>
													</td>
												</tr>
											</table>
										</td>
										<td class="backTablaDer">&#160;</td>
									</tr>
									<tr>
										<td></td>
										<td class="backTablaPie">&#160;</td>
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&#160;</td>
			</tr>
		</table>
	</body>
</html>