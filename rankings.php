<?php
require_once 'admin/php/class/class.socio.php';
require_once 'admin/php/class/class.listado.php';
require_once 'admin/php/minixml/minixml.inc.php';
require_once 'admin/php/generales.php';
require_once 'php/seguridad.php';

if(!isset($_SESSION)) {session_start();}
$idcliente = $_SESSION['QLMSF_idcliente'];

//Verifico que soy nivel superior
if($_SESSION['QLMSF_rango']=='distribuidor' || $_SESSION['QLMSF_rango']=='vendedor' || $_SESSION['QLMSF_rango']=='repositor') {
	header('location:home.php');
}

if(isset($_GET['st']))
	$_SESSION['st'] = $_GET['st'];

//falta armar un array de mis regiones para luego permitir desplegar solo las mias

//me fijo el mes o viene dado o es el actual

//primero veo que sea un mes con anio
if(isset($_GET['m']) && strpos($_GET['m'], '-') !== false){
	$mes = explode('-', $_GET['m'])[0];
	$anio = explode('-', $_GET['m'])[1];
}
else {
	if(isset($_GET['m']) && is_numeric($_GET['m'])) {$mes = $_GET['m'];} else {$mes = intval(date('m'));}
	$anio = date('Y');
}
//traigo todos los distris con sus puntos del mes y totales

// nicolas, chequear esta carga (3 lineas) ya que rompe el style ..si no trae datos
$lst = new listado();
$distris = $lst->getSociosPuntos('distribuidor', intval($mes), intval($anio));
$distris_total = $lst->getSociosPuntos('distribuidor', '', intval($anio));

//inicializo combo de regiones de distri
$regiones = Array();
$regiones['BUENOS AIRES'] = 0;
$regiones['LITORAL'] = 0;
$regiones['NEA'] = 0;
$regiones['NOA'] = 0;

$regiones_prome = $regiones;
$regiones_canti = $regiones;
$regiones_total = $regiones;
$regiones_total_prome = $regiones;

//sumo por region
$fin = count($distris);
for($i=0;$i<$fin;$i++) {
	$regiones[$distris[$i]['equipo']] = $regiones[$distris[$i]['equipo']] + intval($distris[$i]['suma_individual']);
}

//cuento por region
$fin = count($distris);
for($i=0;$i<$fin;$i++) {
	$regiones_canti[$distris[$i]['equipo']] = $regiones_canti[$distris[$i]['equipo']] + 1;
}

//promedio por region mensual
foreach ($regiones as $region => $puntaje) {
	$regiones_prome[$region] = floatval($puntaje/$regiones_canti[$region]);
}

$end = count($distris_total);
for($i=0;$i<$end;$i++) {
	$regiones_total[$distris_total[$i]['equipo']] = $regiones_total[$distris_total[$i]['equipo']] + intval($distris_total[$i]['suma_individual']);
}

//promedio por region total
foreach ($regiones_total as $region => $puntaje) {
	$regiones_total_prome[$region] = floatval($puntaje/$regiones_canti[$region]);
}


//ordeno por regiones de mayor a menor puntaje
arsort($regiones);
arsort($regiones_total);
arsort($regiones_prome);
arsort($regiones_total_prome);


	if(isset($_SESSION['socio'])) {
		$id = $_SESSION['socio'];
		
		$puntosSrv = new PuntosService();
		$puntos_kam = 0;
		$puntos_kas = 0;
		$puntos_supervisores = 0;
		$puntos_vendedores = 0;
		
		if ($_SESSION['QLMSF_rango'] == "kas") {
			$puntos_kam = $puntosSrv->getRankingPuntosKas1($id, $mes);
			$puntos_kas = $puntosSrv->getRankingPuntosKas2($id, $mes);
			$puntos_supervisores = $puntosSrv->getRankingPuntosKas3($id, $mes);
			$puntos_vendedores = $puntosSrv->getRankingPuntosKas4($id, $mes);

			$_SESSION['QLMSF_puntos'] = $puntos_kam[0]->puntos;
		} else if ($_SESSION['QLMSF_rango'] == "kam") {
			$puntos_kas = $puntosSrv->getRankingPuntosKam1($id, $mes);
			$puntos_supervisores = $puntosSrv->getRankingPuntosKam2($id, $mes);
			$puntos_vendedores = $puntosSrv->getRankingPuntosKam3($id, $mes);

			$_SESSION['QLMSF_puntos'] = $puntos_kas[0]->puntos;
		} else if ($_SESSION['QLMSF_rango'] == "supervisor") {
			$puntos_supervisores = $puntosSrv->getRankingPuntosSup1($id, $mes);
			$puntos_vendedores = $puntosSrv->getRankingPuntosSup2($id, $mes);

			$_SESSION['QLMSF_puntos'] = $puntos_supervisores[0]->puntos;
		} else {
			$puntos_vendedores = $puntosSrv->getRankingPuntosVen($id, $mes);
			// $puntos_vendedores = $puntosSrv->getPuntosGroupByRegion($id, $mes);
		}

		$puntos_totales = $puntosSrv->getPuntosGroupByRegionCampeonato();
	}

?>

<section class="gp-ranking-distri">
	<div class="row">
		<div class="col-lg-11">
			<h2>Ranking</h2>
			<ul class="nav nav-tabs">
				<li id="region_solapa_mes" class="active">
					<a onclick="showRegionMes();" href="javascript:void(1);"><?php echo $nombre_mes[$mes]?> <?php echo $anio?></a>
				</li>
			</ul>

			<!--Ranking por region-->
			<div id="region_contenido_mes" class="row" style="padding-top:20px">
				<div class="col-12">
					<div class="panel-group" id="accordion">
						<div class="panel panel-default">
							<div class="panel-body">
								<table class="table table-sm">
									<thead class="thead-light">
										<tr>
											<th width="12%">Nombre</th>
											<th width="12%">Rango</th>
											<th width="1%">Puntos</th>
										</tr>
									</thead>
									<tbody>
										<?php if (count($puntos_kam) > 0) { ?>
											<?php foreach ($puntos_kam as $puntos) { ?>
												<tr>
													<td><?php echo $puntos->nombre ?></td>
													<td><?php echo $puntos->rango ?></td>
													<td><strong><?php echo $puntos->puntos ?> pts +</strong></td>
												</tr>
											<?php } ?>
										<?php } ?>
										<?php if (count($puntos_kas) > 0) { ?>
											<?php foreach ($puntos_kas as $puntos) { ?>
												<tr>
													<td><?php echo $puntos->nombre ?></td>
													<td><?php echo $puntos->rango ?></td>
													<td><strong><?php echo $puntos->puntos ?> pts +</strong></td>
												</tr>
											<?php } ?>
										<?php } ?>
										<?php if (count($puntos_supervisores) > 0) { ?>
											<?php foreach ($puntos_supervisores as $puntos) { ?>
												<tr>
													<td><?php echo $puntos->nombre ?></td>
													<td><?php echo $puntos->rango ?></td>
													<td><strong><?php echo $puntos->puntos ?> pts +</strong></td>
												</tr>
											<?php } ?>
										<?php } ?>
										<?php if (count($puntos_vendedores) > 0) { ?>
											<?php foreach ($puntos_vendedores as $puntos) { ?>
												<tr>
													<td><?php echo $puntos->nombre ?></td>
													<td><?php echo $puntos->rango ?></td>
													<td><strong><?php echo $puntos->puntos ?> pts +</strong></td>
												</tr>
											<?php } ?>
										<?php } ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--Fin Ranking por Region-->

	<section class="gp-ranking-distri">
		<div class="container">
			<div class="panel-group" id="accordion2">
				<div class="panel panel-default">
					<div class="panel-heading">						
						<h2 class="panel-title" style="border-bottom:0; padding:15px 0 0 0; width:16rem">
							<a id="general-ranking-title" style="width:100%">
								<img src="img/icon_desplegar-01.svg" alt="desplegar"/> Ranking general
							</a>
						</h2>
					</div>
					<div id="collapse10" class="collapse">
						<div class="panel-body">
							<ul class="nav nav-tabs">
								<li id="socios_solapa_mes" class="active">
									<a onclick="showSociosMes();" href="javascript:void();">Campeonato</a>
								</li>
							</ul>

							<div id="socios_contenido_mes" class="row" style="padding-top:20px">
								<div class="col-12">
									<div class="panel-group" id="accordion">
										<div class="panel panel-default">
											<div class="panel-body">
												<table class="table table-sm">
													<thead class="thead-light">
														<tr>
															<th width="12%">Región</th>
															<th width="1%">Puntos</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($puntos_totales as $puntos) { ?>
															<tr>
																<td><?php echo $puntos->region ?></td>
																<td><strong><?php echo $puntos->puntos ?> pts +</strong></td>
															</tr>
														<?php } ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="assets/dist/js/bootstrap.bundle.js"></script>


<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-171840062-1');
</script>
<script>
	function showRegionMes() {
		document.getElementById('region_contenido_total').style.display='none';	
		document.getElementById('region_contenido_mes').style.display='flex';	
		document.getElementById('region_solapa_mes').classList.add("active");
		document.getElementById('region_solapa_total').classList.remove("active");	
	}

	function showRegionTotal() {
		document.getElementById('region_contenido_mes').style.display='none';	
		document.getElementById('region_contenido_total').style.display='flex';	
		document.getElementById('region_solapa_mes').classList.remove("active");
		document.getElementById('region_solapa_total').classList.add("active");	
	}

	function showSociosMes() {
		document.getElementById('socios_contenido_total').style.display='none';	
		document.getElementById('socios_contenido_mes').style.display='flex';	
		document.getElementById('socios_solapa_mes').classList.add("active");
		document.getElementById('socios_solapa_total').classList.remove("active");	
	}

	function showSociosTotal() {
		document.getElementById('socios_contenido_mes').style.display='none';	
		document.getElementById('socios_contenido_total').style.display='flex';	
		document.getElementById('socios_solapa_mes').classList.remove("active");
		document.getElementById('socios_solapa_total').classList.add("active");	
	}

	function reloadMes(mes) {
		document.location = 'ranking_distri.php?m='+mes
	}
</script>
