<?php 

    include('db.php');

    $puntos = array();

    $stmt = $dbh->prepare("
        SELECT 
            *
        FROM 
            puntos as p
    ");
    $stmt->execute();
    $puntos = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `puntos` WHERE `identrada`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>