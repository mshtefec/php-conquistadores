<?php

    if (isset($_POST["import"])) {
        
        $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        
        if(in_array($_FILES["file"]["type"], $allowedFileType)) {

            $targetPath = __DIR__ . '\\temp\\' . date('d-m-Y_H_i_s') . '_' . $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

            if ( $xlsx = SimpleXLSX::parse( $targetPath ) ) {

                $sql = "INSERT INTO ganadores (
                    id_socio, 
                    fecha, 
                    concepto, 
                    ranking, 
                    puntaje
                ) VALUES (?,?,?,?,?)";

                $stmt = $conn->prepare($sql);
                
                $stmt->bindParam( 1, $id_socio);
                $stmt->bindParam( 2, $fecha);
                $stmt->bindParam( 3, $concepto);
                $stmt->bindParam( 4, $ranking);
                $stmt->bindParam( 5, $puntaje);

                // voy a quitar la primera fila que son las cabeceras.
                $rows = $xlsx->rows();
                unset($rows[0]);
                
                foreach ( $rows as $key => $fields ) {

                    $id_socio = $fields[0];
                    $fecha = $fields[1];
                    $concepto = $fields[2];
                    $ranking = $fields[3];
                    $puntaje = $fields[4];
                    
                    try {

                        $log_file .= 'Procesando la linea '. $key . PHP_EOL;

                        $stmt->execute();

                        $type = "success";
                        $message = "Excel Importado Correctamente.";
                    } catch (Exception $e) {
                        echo $e;
                    }
                }

                file_put_contents('logs/log_importador_' . date("Y-m-d") . '.txt', $log_file, FILE_APPEND);
                
            } else {
                echo SimpleXLSX::parseError();
            }
            
        } else { 
            $type = "error";
            $message = "Suba un tipo de archivo invalido Excel.";
        }
    }
    
?>