<?php
require_once 'admin/php/class/class.listado.php';
require_once 'admin/php/generales.php';
require_once 'php/seguridad.php';

if(!isset($_SESSION)) {session_start();}

$ls = new listado(0, 10000);

//primero veo que sea un mes con anio
if(isset($_GET['m']) && strpos($_GET['m'], '-') !== false){
	$mes_actual = explode('-', $_GET['m'])[0];
	$anio_actual = explode('-', $_GET['m'])[1];
}
else if(isset($_GET['m'])){

	//me fijo el mes o viene dado o es el actual
	if(isset($_GET['m']) && is_numeric($_GET['m']))
		$mes_actual = $_GET['m'];
	else {
		$lastmes = $ls->getUltimoMes($_SESSION['QLMSF_idcliente']);
		$mes_actual = intval($lastmes);
	}
	$anio_actual = date('Y');
}else {
	$anio_actual = date('Y');
	$mes_actual = intval(date('m'));
}

?>

<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v4.0.1">
		<title>Conquistadores</title>
		
		<!-- Bootstrap core CSS -->
		<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="css/peniaflor.css" rel="stylesheet">

		<link href="assets/front/performance/css/style.css" rel="stylesheet">

	</head>
	<body>
		<script src="assets/dist/js/jquery-3.6.0.min.js"></script>
		<script src="assets/dist/js/bootstrap.bundle.min.js"></script>
		<script src="assets/dist/js/custom.js"></script>

		<?php include_once('cabecera.php');?>

		<main role="main" style="margin-bottom: 4.3rem;">
			<!--barra Mis Datos-->

			<section class="gp-performance-tt">
				<div class="container">
				<h2 style="width:16rem">Mi performance</h2>
					<div class="col-lg-4">
						<h3>SELECCIONE MES</h3>
						<select onchange="reloadMes(this.value);">
							<?php
							$mactual = intval(date('m'));
							$count_meses = 0;
							for($i=$mactual; $i>0; $i--) {
								$count_meses++;
								if($i==$mes_actual)
									echo '<option value="'.$i.'" selected="selected">'.$nombre_mes[$i]. ' ' . date('Y') .'</option>';
								else
									echo '<option value="'.$i.'">'.$nombre_mes[$i]. ' ' . date('Y') .'</option>';
							}
							?>
							<?php
							$i = 12;
							while($count_meses < 12) {

								$count_meses++;
									echo '<option ' . (($_GET['m'] == $i.'-'.(date('Y')-1))?'selected="selected"':'') . ' value="'.$i.'-' . (date('Y') - 1) . '">'.$nombre_mes[$i]. ' ' . (date('Y') - 1) .'</option>';
								$i --;
							}
							?>
						</select>
					</div>

					<nav>
						<div class="nav nav-tabs div_performance" id="nav-tab" role="tablist">
							<a class="nav-link active" id="nav-ventas-tab" data-toggle="tab" href="#nav-ventas" role="tab" aria-controls="nav-ventas" aria-selected="true">Performance Ventas</a>
							<a class="nav-link" id="nav-ejec-tab" data-toggle="tab" href="#nav-ejec" role="tab" aria-controls="nav-ejec" aria-selected="false">Performance Ejecución</a>
							<a class="nav-link" id="nav-ranking-tab" data-toggle="tab" href="#nav-ranking" role="tab" aria-controls="nav-ranking" aria-selected="false">Ranking</a>
						</div>
					</nav>

					<div class="tab-content mi-performance-tabs" id="nav-tabContent">
						<div class="tab-pane fade show active" id="nav-ventas" role="tabpanel" aria-labelledby="nav-ventas-tab">
							<?php include_once('performance_ventas.php');?>
						</div>
						<div class="tab-pane fade" id="nav-ejec" role="tabpanel" aria-labelledby="nav-ejec-tab">
							<?php include_once('performance_ejecucion.php');?>
						</div>
						<div class="tab-pane fade" id="nav-ranking" role="tabpanel" aria-labelledby="nav-ranking-tab">
							<?php include_once('rankings.php');?>
						</div>
					</div>
				</div>
			</section>
			
		</main>

		<?php include_once('pie.php');?>

		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
		<script src="assets/dist/js/bootstrap.bundle.js"></script>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171840062-1"></script>
		
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-171840062-1');
		</script>

		<script>
			function navVentas() {
				document.getElementById('nav-ventas-tab').classList.add("active");
				document.getElementById('nav-ejec-tab').classList.remove("active");
				document.getElementById('nav-ranking-tab').classList.remove("active");

				document.getElementById('nav-ventas').classList.add("show active");
				document.getElementById('nav-ejec').classList.remove("show active");
				document.getElementById('nav-ranking').classList.remove("show active");
			}

			function navEjecucion() {
				document.getElementById('nav-ventas-tab').classList.remove("active");
				document.getElementById('nav-ejec-tab').classList.add("active");
				document.getElementById('nav-ranking-tab').classList.remove("active");

				document.getElementById('nav-ventas').classList.remove("show active");
				document.getElementById('nav-ejec').classList.add("show active");
				document.getElementById('nav-ranking').classList.remove("show active");
			}

			function reloadMes(mes) {
				document.location = 'mi_performance.php?m='+mes
			}
		</script>
	</body>
</html>
