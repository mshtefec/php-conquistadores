<?php 

    include('scripts/db.php');

    $ganadores = array();

    $stmt = $dbh->prepare("
        SELECT 
            g.id_socio as id,
            g.concepto as concepto,
            g.ranking as ranking,
            g.puntaje as puntaje,
            CONCAT(s.apellido, ', ', s.nombre) as nombre,
            s.region as region,
            s.rango as rango,
            DATE_FORMAT(g.fecha, '%M') as mes, 
            YEAR(g.fecha) as 'año'
        FROM 
            ganadores as g
        JOIN
            socios as s ON (g.id_socio = s.id)
    ");
    $stmt->execute();
    $ganadores = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `ganadores` WHERE `id`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>

<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Listado de Ganadores
                    <a type="button" class="btn btn-success" href="new.php"><i class="bi bi-plus-circle"></i> Nuevo Ganador</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Administración de Ganadores</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Listado de Ganadores cargados</h6>

                    <?php if ($ganadores && !empty($ganadores)) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Apellido y Nombre</th>
                                    <th scope="col">Region</th>
                                    <th scope="col">Rango</th>
                                    <th scope="col">Mes</th>
                                    <th scope="col">Año</th>
                                    <th scope="col">Concepto</th>
                                    <th scope="col">Ranking</th>
                                    <th scope="col">Puntaje</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ganadores as $g) { ?>
                                    <tr>
                                        <th scope="row"><?php echo $g['id'] ?></th>
                                        <td><?php echo $g['nombre'] ?></td>
                                        <td><?php echo $g['region'] ?></td>
                                        <td><?php echo $g['rango'] ?></td>
                                        <td><?php echo $g['mes'] ?></td>
                                        <td><?php echo $g['año'] ?></td>
                                        <td><?php echo $g['concepto'] ?></td>
                                        <td><?php echo $g['ranking'] ?></td>
                                        <td><?php echo $g['puntaje'] ?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <a type="button" class="btn btn-outline-primary" href="show.php?id=<?php echo $g['id']?>"><i class="bi bi-search"></i> Ver</a>
                                                <button type="button" class="btn btn-outline-warning"><i class="bi bi-pen"></i> Editar</button>
                                                <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deletedModal"><i class="bi bi-trash"></i> Eliminar</button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="text-center">
                            <h5>No existen datos para mostrar</h5>
                        </div>
                    <?php } ?>
                </div>
                <div class="card-footer">
                    <a type="button" class="btn btn-warning" href="importador.php"><i class="bi bi-plus-circle"></i> Importar Ganadores</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Alert Deleted -->
<div class="modal fade" id="deletedModal" tabindex="-1" aria-labelledby="deletedModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="deletedModalLabel">Estás seguro que desea elimiar?</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Estos cambios no pueden ser devueltos.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Salir</button>
        <a type="button" class="btn btn-outline-danger" href="index.php?id=<?php echo $g['id']?>"><i class="bi bi-trash"></i> Eliminar</a>
      </div>
    </div>
  </div>
</div>

<?php include('../index/footer.php'); ?>

