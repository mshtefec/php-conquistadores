<?php return array(
    'root' => array(
        'pretty_version' => 'dev-develop',
        'version' => 'dev-develop',
        'type' => 'composer-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '12159b2a2bb670731a273f0c514da6d6fe7fdb7f',
        'name' => 'max-note-cpce/utils',
        'dev' => true,
    ),
    'versions' => array(
        'max-note-cpce/utils' => array(
            'pretty_version' => 'dev-develop',
            'version' => 'dev-develop',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '12159b2a2bb670731a273f0c514da6d6fe7fdb7f',
            'dev_requirement' => false,
        ),
        'shuchkin/simplexlsx' => array(
            'pretty_version' => '0.8.25',
            'version' => '0.8.25.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../shuchkin/simplexlsx',
            'aliases' => array(),
            'reference' => '7dfacfbe51137ad08e047cd5ac57eef26158d120',
            'dev_requirement' => false,
        ),
    ),
);
