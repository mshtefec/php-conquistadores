<?php
require_once 'admin/php/class/class.listado.php';
require_once 'php/seguridad.php';
require_once 'php/seguridad_email.php';
// para seccion novedades
require_once 'admin/php/generales.php';

if(!isset($_SESSION)) {session_start();}

$ls = new listado(0, 10000);

// para seccion novedades
$lsN = new listado(0, 10000);
$novedades = $lsN->getNoticiasFront();
$nro = count($novedades);

//inicio variables
$fil_min	= '';
$fil_max	= '';
$fil_cat	= '';
$fil_tp		=	'';
$fil_bus	= '';

//recupero del post de buscador
if(isset($_POST['fil_min'])) 	{$fil_min = $_POST['fil_min'];}
if(isset($_POST['fil_max']))	{$fil_max = $_POST['fil_max'];}
if(isset($_POST['fil_cat']))	{$fil_cat = $_POST['fil_cat'];}
if(isset($_POST['fil_tp'])) 	{$fil_tp 	= $_POST['fil_tp'];}
if(isset($_POST['fil_bus'])) 	{$fil_bus = $_POST['fil_bus'];}

// $categorias 	= $ls->getCampaniasCombo();
// $nro_c				= count($categorias);
// $premios 			= $ls->getPremiosHome($fil_min, $fil_max, $fil_cat, $fil_tp, $fil_bus);
// $nro_p				= count($premios);
// $premios_pop	= $ls->getPremiosHomePopulares();
// $nro_pop			= count($premios_pop);
// $premios_alc	= $ls->getPremiosHomeAlcance($_SESSION['QLMSF_puntos']);
// $nro_alc			= count($premios_alc);
// $premios_new	= $ls->getPremiosHomeNuevos();
// $nro_new			= count($premios_new);
// $premios_des	= $ls->getPremiosHomeDestacados();
// $nro_des			= count($premios_des);
?>
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Aper.net">
    <title>Conquistadores</title>
    <!--link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/"-->
    <!-- Bootstrap core CSS -->
		<link href="assets/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/peniaflor.css" rel="stylesheet">
  </head>
  <body>
    	<?php 
			include_once('cabecera.php');
		?>
		<main role="main" >

			<section class="container-lg-home my-2">
			<div class="container p-0">
				
			
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<!--li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
                        <li data-target="#myCarousel" data-slide-to="7"></li-->
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="img/slides/1.png" width="850" height="340" alt="Conquistadores" class="gp-slide-dk"/>
							<img src="img/slides/1.png" width="100%" alt="Conquistadores" class="gp-slide-mb"/>   
						</div>
						<div class="carousel-item">
							<img src="img/slides/2.png" width="850" height="340" alt="Conquistadores" class="gp-slide-dk"/>
							<img src="img/slides/2.png" width="100%" alt="Conquistadores" class="gp-slide-mb"/>  
						</div>
						<!--<div class="carousel-item">
							<img src="img/slides/3.png" width="850" height="340" alt="Conquistadores" class="gp-slide-dk"/>
							<img src="img/slides/3.png" width="100%" alt="Conquistadores" class="gp-slide-mb"/>  
						</div>
                        <div class="carousel-item">
							<img src="img/slides/4.png" width="850" height="340" alt="Conquistadores" class="gp-slide-dk"/>
							<img src="img/slides/4.png" width="100%" alt="Conquistadores" class="gp-slide-mb"/>  
						</div>-->
					</div>
					<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
				<div class="gp-banner-destacado">
					<div class="text-left">
						<h2>¡Bienvenido!</h2>
						<p>Te invitamos a seguirnos en<br><img src="img/icon_instagram.svg" alt="Instagram" width="18"/>
						<a href="https://www.instagram.com/conquistadordelpdv/">@conquistadordelpdv</a> 
						</p>
						<p> para mantenerte al tanto de las novedades.</p>
					</div>
				</div>
			</div>	
			</section>

			<section class="gp-novedades">
				<div class="container">
					<h2>Novedades</h2>
					<div class="row">
						<?php 
						if($nro>0){
							for($i=0;$i<3;$i++) {
						?>
						<div class="col-lg-4">
							<a href="novedades_detalle.php?n=<?php echo $novedades[$i]['idnoticia'];?>" class="gp-caja-nota">
								<div class="foto">
									<?php
									if($novedades[$i]['imagen1'] != '')
										echo '<img src="archivos/'.$novedades[$i]['imagen1'].'" width="100%" alt="'.$novedades[$i]['titulo'].'"/>';
									else
										echo '<img src="img/no-foto.jpg" width="100%" alt="'.$novedades[$i]['titulo'].'"/>';
									?>
								</div>
								<h3><?php echo $novedades[$i]['titulo'];?></h3>
								<p>
									<?php
									//limito el cuerpo de la novedad a 200 caracteres
									if($novedades[$i]['cuerpo'] != ''){
										if(strlen($novedades[$i]['cuerpo'])>200)
											$cuerpo = myTruncate($novedades[$i]['cuerpo'],200);
										else
											$cuerpo = $novedades[$i]['cuerpo'];
									}
									else	$cuerpo = '';
									echo nl2br($cuerpo);
									?>
								</p>
							</a>
						</div>
						<?php 
							}
						} else echo 'A&#218;N NO SE HAN CARGAGO NOVEDADES';
						?>
					</div>
					<!-- /.row -->
				</div><!-- /.container -->
			</section>


<!--   
			<section class="container-lg gp-buscar">
				<div class="container">
					<div class="row">
						<form name="sentMessage" id="contactForm" method="POST" action="catalogo.php" novalidate class="form-inline">
							<div class="form-group col-lg-2">
								<h3>Buscar en catálogo</h3>
							</div>
							<div class="form-group col-lg-4">
								<label for="puntosdesde">Entre</label>
								<input class="form-control gp-pts" type="text" name="fil_min" id="fil_min" value="<?php echo $fil_min?>">
								<label for="puntoshasta">y</label>
								<input class="form-control gp-pts" type="text" name="fil_max" id="fil_max" value="<?php echo $fil_max?>">
								<span>millas</span>
							</div>
							<div class="form-group col-lg-4">
								<input class="form-control gp-busca" type="text" name="fil_bus" id="fil_bus" value="<?php echo $fil_bus?>" placeholder="Ingrese palabra">
							</div>
							<div class="form-group col-lg-2"></div>
							<div class="form-group col-lg-2 gp-mgtop10 gp-mb-oculta"></div>
							<div class="form-group col-lg-4 gp-mgtop10">   
								<select class="form-control gp-tipo" name="fil_tp" id="fil_tp">
									<option value="" <?php if($fil_tp == '') echo 'selected = "selected"';?>>Tipo de premio</option>
									<option value="giftcard virtual y sucursales" <?php if($fil_tp == 'giftcard virtual y sucursales') echo 'selected = "selected"';?>>Online y sucursal</option>
									<option value="giftcard solo sucursales" <?php if($fil_tp == 'giftcard solo sucursales') echo 'selected = "selected"';?>>Sólo sucursal</option>
								</select>
							</div>
							<div class="form-group col-lg-4 gp-mgtop10">
								<select class="form-control gp-select" name="fil_cat" id="fil_cat">
									<option value="" <?php if($fil_cat == '') echo 'selected="selected"';?>>Todas las categorías</option>
									<?php
									for($i=0;$i<$nro_c;$i++){
										if($fil_cat == $categorias[$i]['idcampania'])
											echo '<option value="'.$categorias[$i]['idcampania'].'" selected="selected">'.$categorias[$i]['nombre'].'</option>';
										else
											echo '<option value="'.$categorias[$i]['idcampania'].'">'.$categorias[$i]['nombre'].'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group col-lg-2 gp-mgtop10">  
								<button type="submit" class="btn btn-primary gp-btn-rojo-borde">Buscar</button>
							</div>
						</form>
					</div>
				</div>
			</section>

			<div class="gp-franja-lineas">&#160;</div>

			<section class="gp-destacados gp-premios">
				<div class="container">
					<h2>Destacados</h2>
					<div class="row">
						<?php 
						if($nro_des>0){
							for($i=0;$i<$nro_des;$i++) {?>
						<div class="col-lg-3">
							<a href="detalle_premio.php?idp=<?php echo $premios_des[$i]['idpremio'];?>" class="gp-caja-premio">
								<span><?php echo $premios_des[$i]['categoria'];?></span>
								<div class="foto"><img src="archivos/<?php echo $premios_des[$i]['imagen'];?>" width="100%" alt="<?php echo $premios_des[$i]['nombre'];?>"/></div>
								<p><?php echo $premios_des[$i]['nombre'];?></p>
								<h3><?php echo $premios_des[$i]['valor'];?> millas</h3>
							</a>
						</div>
						<?php 
							}
						} else echo 'A&#218;N NO SE HAN CARGAGO PREMIOS DESTACADOS';
						?>
					</div>
				</div>
			</section>

			<section class="gp-populares gp-premios">
				<div class="container">
					<h2>Últimos agregados</h2>
					<div class="row">
						<?php 
						if($nro_new>0){
							for($i=0;$i<$nro_new;$i++) {?>
						<div class="col-lg-3">
							<a href="detalle_premio.php?idp=<?php echo $premios_new[$i]['idpremio'];?>" class="gp-caja-premio">
								<span><?php echo $premios_new[$i]['categoria'];?></span>
								<div class="foto"><img src="archivos/<?php echo $premios_new[$i]['imagen'];?>" width="100%" alt="<?php echo $premios_new[$i]['nombre'];?>"/></div>
								<p><?php echo $premios_new[$i]['nombre'];?></p>
								<h3><?php echo $premios_new[$i]['valor'];?> millas</h3>
							</a>
						</div>
						<?php 
							}
						} else echo 'A&#218;N NO SE HAN CARGAGO PREMIOS';
						?>
					</div>
					
				</div>
			</section>
  
		 	 <section class="gp-franja-banner">
             <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="carousel-item active">
                  <img src="img/banner-home1.png" alt="GP2020"/>
                  </div>
                  <div class="carousel-item">
                    <img src="img/banner-home2.png" alt="GP2020"/>
                  </div>
                   <div class="carousel-item">
                    <img src="img/banner-home3.png" alt="GP2020"/>
                  </div>
                   <div class="carousel-item">
                    <img src="img/banner-home4.png" alt="GP2020"/>
                  </div>
                </div>
              </div>
             </section>
 
			<section class="gp-populares gp-premios">
				<div class="container">
					<h2 style="width:13.5rem;">Más populares</h2>
					<div class="row">
						<?php 
						if($nro_pop>0){
							for($i=0;$i<$nro_pop;$i++) {?>
						<div class="col-lg-3">
							<a href="detalle_premio.php?idp=<?php echo $premios_pop[$i]['idpremio'];?>" class="gp-caja-premio">
								<span><?php echo $premios_pop[$i]['categoria'];?></span>
								<div class="foto"><img src="archivos/<?php echo $premios_pop[$i]['imagen'];?>" width="100%" alt="<?php echo $premios_pop[$i]['nombre'];?>"/></div>
								<p><?php echo $premios_pop[$i]['nombre'];?></p>
								<h3><?php echo $premios_pop[$i]['valor'];?> millas</h3>
							</a>
						</div>
						<?php 
							}
						} else echo 'A&#218;N NO HAY PREMIOS M&#193;S ELEGIDOS';
						?>
					</div>
				</div>
			</section>
			 -->
		</main>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    	<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
		<script src="assets/dist/js/bootstrap.bundle.js"></script>

		<?php include_once('pie.php');?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171840062-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-171840062-1');
		</script>
	</body>
</html>
