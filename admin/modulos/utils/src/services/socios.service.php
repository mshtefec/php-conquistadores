<?php
    require_once("autoload.php");
    
    class SociosService extends Conexion {
        private $conexion;

        public function __construct() {
            $this->conexion = new Conexion();
            $this->conexion = $this->conexion->connect();
        }

        public function getSocios() {
            $handler = $this->conexion->query("SELECT * FROM socios");
            
            return $handler->fetchAll(PDO::FETCH_OBJ);
        }

        public function getSocio($id) {
            $handler = $this->conexion->prepare("SELECT * FROM socios WHERE id = :id");
            $handler->bindParam(':id', $id, PDO::PARAM_INT);
            $handler->execute();

            return $handler->fetch(PDO::FETCH_OBJ);
        }
    }
?>