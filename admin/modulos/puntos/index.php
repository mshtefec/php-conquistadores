<?php 

    include('scripts/db.php');

    $puntos = array();

    if(isset($_GET['concepto'])) {
        $concepto = $_GET['concepto'];

        if ($concepto == 'todos') {
            $stmt = $dbh->prepare("
                SELECT 
                    *
                FROM 
                    puntos as p
            ");
        } else {
            $stmt = $dbh->prepare("
                SELECT 
                    *
                FROM 
                    puntos as p
                WHERE 
                    p.concepto = :concepto
            ");

            $stmt->bindParam(':concepto', $concepto, PDO::PARAM_INT);
            $concepto = trim($_GET['concepto']);
        }
    } else {
        $concepto = 'todos';
        
        $stmt = $dbh->prepare("
            SELECT 
                *
            FROM 
                puntos as p
        ");
    }

    $stmt->execute();
    $puntos = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `puntos` WHERE `id`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>

<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Listado de Puntos
                    <a type="button" class="btn btn-success" href="new.php"><i class="bi bi-plus-circle"></i> Nuevo Punto</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Administración de Puntos</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Listado de Puntos cargados</h6>

                    <form name="filter" action="" method="get">
                        <div class="col-md-3">
                            <label for="concepto" class="form-label">Filtro por Concepto</label>
                            <select class="form-select" id="concepto" name="concepto" required="" onchange="this.form.submit()">
                                <option value="todos" <?php if($concepto == "todos"){ echo " selected"; }?> >Todos</option>
                                <option value="ventas" <?php if($concepto == "ventas"){ echo " selected"; }?> >Ventas</option>
                                <option value="ejecucion" <?php if($concepto == "ejecucion"){ echo " selected"; }?> >Ejecución</option>
                            </select>
                        </div>
                    </form>
                    <br>

                    <?php if ($puntos && !empty($puntos)) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Linea</th>
                                    <th scope="col">Puntos</th>
                                    <th scope="col">Concepto</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($puntos as $p) { ?>

                                    <tr>
                                        <th scope="row"><?php echo $p['id'] ?></th>
                                        <td><?php echo $p['linea'] ?></td>
                                        <td><?php echo $p['puntos'] ?></td>
                                        <td><?php echo $p['concepto'] ?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <a type="button" class="btn btn-outline-primary" href="show.php?id=<?php echo $p['id']?>"><i class="bi bi-search"></i> Ver</a>
                                                <button type="button" class="btn btn-outline-warning"><i class="bi bi-pen"></i> Editar</button>
                                                <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deletedModal-<?php echo $p['id']?>"><i class="bi bi-trash"></i> Eliminar</button>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- Alert Deleted -->
                                    <div class="modal fade" id="deletedModal-<?php echo $p['id']?>" tabindex="-1" aria-labelledby="deletedModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deletedModalLabel">Estás seguro que desea elimiar?</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                Estos cambios no pueden ser devueltos.
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Salir</button>
                                                <a type="button" class="btn btn-outline-danger" href="index.php?id=<?php echo $p['id']?>"><i class="bi bi-trash"></i> Eliminar</a>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="text-center">
                            <h5>No existen datos para mostrar</h5>
                        </div>
                    <?php } ?>
                </div>
                <div class="card-footer">
                    <a type="button" class="btn btn-warning" href="importador.php"><i class="bi bi-plus-circle"></i> Importar Puntos</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>