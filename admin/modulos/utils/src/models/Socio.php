<?php

    class Socio {
        
        private $id;
        private $nombre;
        private $apellido;
        private $rango;
        private $clave;
        private $region;
        private $distribuidora;
        private $email;
        private $direccion;
        private $localidad;
        private $provincia;
        private $telefono;
        private $fecha_carga;
        private $lastlog;

        public function __contruct($nombre) {
            $this->nombre = $nombre;
        }

        public function getNombre() {
            return $this->nombre;
        }

        public function setNombre($nombre) {
            $this->nombre = $nombre;
        }
    }
?>
