<div class="row">
    <div class="col-lg-4">
        <h3>
            <?php echo strtoupper($nombre_mes[$mes_actual]).' '.$anio_actual?>
        </h3>
        <ul>
            <li>Puntos sumadas en el mes <strong><?php echo $mis_millas;?></strong></li>
            <li>Posición en el mes <strong><?php echo $mi_posicion_mes;?></strong></li>
            <li>Posición en el campeonato <strong><?php echo $mi_posicion_campeonato;?></strong></li>
        </ul>
    </div>
    <div class="col-lg-4">
        <h3>PODIO DEL MES</h3>
        <ul>
            <?php
            for($i=0;$i<3;$i++){
                if($ranking_mes[$i]['nombre']!='')
                    $socio = $ranking_mes[$i]['nombre'].' '.$ranking_mes[$i]['apellido'];
                else
                    $socio = $ranking_mes[$i]['apellido'];
                if($ranking_mes[$i]['grupo']==1){
                    switch($ranking_mes[$i]['equipo']) {
                        case 'AMBA':
                            $equipo = 'AMBA';
                            break;
                        case 'CENTRO':
                            $equipo = 'CENTRO';
                            break;
                        case 'CUYO':
                            $equipo = 'CUYO';
                            break;
                        case 'LITORAL':
                            $equipo = 'LITORAL';
                            break;
                        case 'NEA':
                            $equipo = 'NEA';
                            break;
                        case 'NOA':
                            $equipo = 'NOA';
                            break;
                        case 'PBA':
                            $equipo = 'PBA';
                            break;
                        case 'SUR':
                            $equipo = 'SUR';
                            break;
                        default: $equipo = '';
                    }
                } else {
                    switch($ranking_mes[$i]['equipo']) {
                        case 'GBA NOROESTE':
                            $equipo = 'GBA NOROESTE';
                            break;
                        case 'CAPITAL2':
                            $equipo = 'CAPITAL2';
                            break;
                        case 'LITORAL':
                            $equipo = 'LITORAL';
                            break;
                        case 'GBA NORTE':
                            $equipo = 'GBA NORTE';
                            break;
                        case 'CENTRO':
                            $equipo = 'CENTRO';
                            break;
                        case 'GBA SUR2':
                            $equipo = 'GBA SUR2';
                            break;
                        case 'CAPITAL1':
                            $equipo = 'CAPITAL1';
                            break;
                        case 'LA PLATA':
                            $equipo = 'LA PLATA';
                            break;
                        default: $equipo = '';
                    }
                }
            ?>	
            <li><strong><?php echo $i + 1;?>.</strong> <?php echo strtoupper($socio);?> / <strong><?php echo $equipo;?></strong></li>
            <?php }?>
        </ul>
    </div>
    
</div>

<div class="row mt-5">
    <h2>Objetivos</h2>
    <div class="row" style="padding-top:20px">
        <div class="col-lg-12">
            <table class="table table-sm">
                <thead class="thead-light">
                    <tr>
                        <th width="26%">
                            <?php 
                            
                            if($_SESSION['QLMSF_rango'] == 'repositor') {
                                echo 'Incentivo';
                            }
                            else 
                                echo 'Línea comercial';
                            
                            ?>
                            
                        </th>
                        <th width="12%">Objetivo</th>
                        <th width="12%">Avance</th>
                        <th width="12%">Puntos</th>
                        <th width="38%" style="text-align:center">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    for($i=0;$i<$nro_lineas;$i++){
                        $linea[$i] = $ls->getObjetivosMes($mes_actual,$anio_actual,$_SESSION['QLMSF_idcliente'],$lineas_comerciales_mes[$i]['linea_comercial']);
                        $objetivo = $linea[$i][0]['objetivo'];
                        $suma = round($linea[$i][0]['suma'], 1);
                        $avance = round($linea[$i][0]['avance'], 1);
                        $avance_por = round($linea[$i][0]['avance_por'], 1);
                        switch($avance_por){
                            case ($avance_por >= 0 && $avance_por <= 85):
                                $color = 'rojo'; break;
                            case ($avance_por >= 85.1 && $avance_por <= 99.9):
                                $color = 'amarillo'; break;
                            case ($avance_por >= 100):
                                $color = 'verde'; break;
                        }
                    ?>
                    <tr>
                        <td><strong><?php echo strtoupper($lineas_comerciales_mes[$i]['linea_comercial']);?></strong></td>
                        <td><?php echo $objetivo;?></td>
                        <td><?php echo $avance;?></td>
                        <td><?php echo $suma;?></td>
                        <td align="center">
                            <div class="barra">
                                <img src="img/barra-<?php echo $color;?>.png" width="<?php if($avance_por <= 100) echo $avance_por; else echo '100';?>%" height="15" alt="<?php echo $avance_por;?>%"/>
                            </div> 
                            <span><?php echo $avance_por;?>%</span>
                        </td>
                    </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
        
    </div>
    <div class="row" style="padding-top:20px">  
        <div class="col-3 col-lg-1"><img src="img/icon_meta-01.svg" alt="¿Como sumar puntos?"/></div>
        <div class="col-9 col-lg-6 gp-bajada">
            <h4><strong>¿Cómo se calculan las puntos?</strong></h4>
            <?php 
            
            if($_SESSION['QLMSF_rango'] != 'repositor'){
            
            ?>
            Cada incentivo tendrá sus propios objetivos, pero la
            META de todos siempre será lograr la mejor ejecución en
            cada punto de venta!<br>
            Asique prepara tus neumáticos y salí a romper la pista en
            busca de la mejor góndola, punteras, exhibiciones
            adicionales y más!<br>
            Cada VUELTA que das a la pista es una EJECUCIÓN
            REALIZADA.<br>
            Cada incentivo tiene premios distintos y serán
            comunicados por su supervisor/ejecutivo.<br>
            <?php 
            }else {
                ?>
                Cada incentivo tendrá sus propios objetivos, pero la META de todos siempre será lograr la mejor ejecución en cada punto de venta!<br>
                Asique prepara tus neumáticos y salí a romper la pista en busca de la mejor góndola, punteras, exhibiciones adicionales y más!<br>
                Cada VUELTA que das a la pista es una EJECUCIÓN REALIZADA<br />
                <?php
            }
            ?>
        </div>
        <?php
        if($_SESSION['QLMSF_rango'] != 'repositor'){
        ?>
        <div class="col-12 col-lg-5 gp-bajada">
            <h4>&nbsp;</h4>
            SUMAS PUNTOSPOR EL CUMPLIMIENTO DE OBJETIVOS:<br />
            •        Si llegas a un alcance entre 85% y 99% sumas el 50% de las puntos<br />
            •        Si llegas al objetivo de 100% sumas el 100% de las puntos<br />
            •        Y si superas el objetivo tenes puntos extras (tope en 120%) 
        </div>
        <?php 
        }
        ?>
    </div>
</div>
