<?php 

    include('db.php');

    $socios = array();

    $stmt = $dbh->prepare("
        SELECT 
            *
        FROM 
            socios as s
    ");
    $stmt->execute();
    $socios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `socios` WHERE `idsocio`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>