<?php 

    session_start();

    include('scripts/db.php');

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        
        $id = $_POST['id'];
        $region = $_POST['region'];
        $rango = $_POST['rango'];
        $nombre = $_POST['nombre'];
        $apellido = $_POST['apellido'];
        $email = $_POST['email'];
        $clave = $_POST['clave'];
        $distribuidora = $_POST['distribuidora'];
        $direccion = $_POST['direccion'];
        $localidad = $_POST['localidad'];
        $provincia = $_POST['provincia'];
        $telefono = $_POST['telefono'];

        $stmt = $dbh->prepare("
            INSERT INTO socios (id, region, rango, nombre, apellido, email, clave, distribuidora, direccion, localidad, provincia, telefono) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)
        ");

        $stmt->execute([
            $id,
            $region,
            $rango,
            $nombre,
            $apellido,
            $email,
            $clave,
            $distribuidora,
            $direccion,
            $localidad,
            $provincia,
            $telefono
        ]);

        header("Location: index.php");
    }

?>


<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Nuevo de Socio
                    <a type="button" class="btn btn-primary" href="index.php"><i class="bi bi-arrow-return-left"></i> Volver a Socios</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Nuevo Socio</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Nuevo de Socio</h6>
                    
                    <form action="new.php" method="post">
                        <hr class="my-4">

                        <div class="row g-3">
                            <div class="col-md-4">
                                <label for="id" class="form-label">Codigo</label>
                                <input type="text" class="form-control" id="id" name="id">
                            </div>
                            <div class="col-md-4">
                                <label for="region" class="form-label">Region</label>
                                <input type="text" class="form-control" id="region" name="region">
                            </div>
                            <div class="col-md-4">
                                <label for="rango" class="form-label">Rango</label>
                                <input type="text" class="form-control" id="rango" name="rango">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="nombre" class="form-label">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre">
                            </div>
                            <div class="col-md-3">
                                <label for="apellido" class="form-label">Apellido</label>
                                <input type="text" class="form-control" id="apellido" name="apellido">
                            </div>
                            <div class="col-md-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>
                            <div class="col-md-3">
                                <label for="clave" class="form-label">Clave</label>
                                <input type="text" class="form-control" id="clave" name="clave">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-3">
                                <label for="distribuidora" class="form-label">Distribuidora</label>
                                <input type="text" class="form-control" id="distribuidora" name="distribuidora">
                            </div>
                            <div class="col-md-3">
                                <label for="direccion" class="form-label">Direccion</label>
                                <input type="text" class="form-control" id="direccion" name="direccion">
                            </div>
                            <div class="col-md-3">
                                <label for="localidad" class="form-label">Localidad</label>
                                <input type="text" class="form-control" id="localidad" name="localidad">
                            </div>
                            <div class="col-md-3">
                                <label for="provincia" class="form-label">Provincia</label>
                                <input type="text" class="form-control" id="provincia" name="provincia">
                            </div>
                        </div>
                        <br>
                        <div class="row g-3">
                            <div class="col-md-4">
                                <label for="telefono" class="form-label">Telefono</label>
                                <input type="text" class="form-control" id="telefono" name="telefono">
                            </div>
                        </div>

                        <hr class="my-4">

                        <button class="w-100 btn btn-success btn" type="submit">Guardar</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>