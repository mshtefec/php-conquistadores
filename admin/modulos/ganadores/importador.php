<?php
    include_once("../utils/utils.php");
    include_once("../utils/src/importador_ganadores.php");
?>

<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <h5>
            <span class="badge bg-warning text-dark">Consideraciones</span>
        </h5>
        <p class="h6">
            Importante a tener en cuenta el siguiente formato para el importador excel.<br>
            Los archivos deben ser con extensi&oacute;n <span class="badge bg-secondary">.xls</span> o <span class="badge bg-secondary">.xlsx</span>.<br>
            Adem&aacute;s deben respetar las siguientes reglas, donde los campos marcados con asterisco deben ser completados obligatoriamente y se debe mantener el mismo orden:
            <ul class="list-unstyled h6">
                <li><strong>Formato de Columnas:</strong>
                    <ul>
                        <li><span class="badge rounded-pill bg-light text-dark">*ID_SOCIO:</span> (clave num&eacute;rica), longitud 8 caracteres. Corresponde con el D.N.I. del Socio y debe estar cargado antes sin excepciones.</li>
                        <li><span class="badge rounded-pill bg-light text-dark">*FECHA:</span> (fecha), debe ser dd/mm/YYYY sin excepciones. Ejemplo 01/08/2021.</li>
                        <li><span class="badge rounded-pill bg-light text-dark">*CONCEPTO:</span> (alfanum&eacute;rica con espacios), longitud indefinida.</li>
                        <li><span class="badge rounded-pill bg-light text-dark">*RANKING:</span> (num&eacute;rica), longitud 9999.</li>
                        <li><span class="badge rounded-pill bg-light text-dark">*PUNTOS:</span> (num&eacute;rica), longitud 999.</li>
                    </ul>
                </li>
            </ul>
        </p>
        <div class="alert alert-secondary" role="alert">
            En cualquier situaci&oacute;n siempre puede optar por <a href="csv/IMPORTADOR_GANADORES.xlsx" class="alert-link">Descargar aqu&iacute; el archivo de ejemplo</a>. y simplemente cambiar los valores.
        </div>
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Importador Formato Excel (.xls - .xlsx).
                </div>
                <div class="card-body">
                    <h5 class="card-title">Modulo de Importación de Ganadores</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Sube un archivo Excel con formato (.xls - .xlsx)</h6>

                    <div class="outer-container">
                        <form action="" method="post"
                            name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
                            <div>
                                <input type="file" name="file"id="file" accept=".xls,.xlsx">
                                <button type="submit" id="submit" name="import" class="btn-submit">Importar</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-footer">
                    <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
                    <a type="button" class="btn btn-info" href="index.php"><i class="bi bi-arrow-left-circle"></i> Volver al Listado de Ganadores</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>