<?php 

    include('scripts/db.php');

    $socios = array();

    if(isset($_GET['rango'])) {
        $rango = $_GET['rango'];

        if ($rango == 'todos') {
            $stmt = $dbh->prepare("
                SELECT 
                    *
                FROM 
                    socios as s
            ");
        } else {
            $stmt = $dbh->prepare("
                SELECT 
                    *
                FROM 
                    socios as s
                WHERE 
                    s.rango = :rango
            ");

            $stmt->bindParam(':rango', $rango, PDO::PARAM_INT);
            $rango = trim($_GET['rango']);
        }
    } else {
        $rango = 'todos';
        
        $stmt = $dbh->prepare("
            SELECT 
                *
            FROM 
                socios as s
        ");
    }

    $stmt->execute();
    $socios = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $query = "
            DELETE FROM `socios` WHERE `idsocio`=:id
        ";

        $stmt = $dbh->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();

        header("Location: index.php");
    }

?>

<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Listado de Socios
                    <a type="button" class="btn btn-success" href="new.php"><i class="bi bi-plus-circle"></i> Nuevo Socio</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Administración de Socios</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Listado de Socios cargados</h6>

                    <form name="filter" action="" method="get">
                        <div class="col-md-3">
                            <label for="rango" class="form-label">Filtro por Rango</label>
                            <select class="form-select" id="rango" name="rango" required="" onchange="this.form.submit()">
                                <option value="todos" <?php if($rango == "todos"){ echo " selected"; }?> >Todos</option>
                                <option value="vendedor" <?php if($rango == "vendedor"){ echo " selected"; }?> >Vendedores</option>
                                <option value="supervisor" <?php if($rango == "supervisor"){ echo " selected"; }?> >Supervisores</option>
                                <option value="kam" <?php if($rango == "kam"){ echo " selected"; }?> >Kam</option>
                                <option value="kas" <?php if($rango == "kas"){ echo " selected"; }?> >Kas</option>
                            </select>
                        </div>
                    </form>
                    <br>

                    <?php if ($socios && !empty($socios)) { ?>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Id (DNI)</th>
                                    <th scope="col">Apellido y Nombre</th>
                                    <th scope="col">Rango</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($socios as $s) { ?>

                                    <tr>
                                        <th scope="row"><?php echo $s['id'] ?></th>
                                        <td><?php echo $s['apellido'] . ', ' . $s['nombre'] ?></td>
                                        <td><?php echo $s['rango'] ?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <a type="button" class="btn btn-outline-primary" href="show.php?id=<?php echo $s['id']?>"><i class="bi bi-search"></i> Ver</a>
                                                <button type="button" class="btn btn-outline-warning"><i class="bi bi-pen"></i> Editar</button>
                                                <button type="button" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#deletedModal-<?php echo $s['id']?>"><i class="bi bi-trash"></i> Eliminar</button>
                                            </div>
                                        </td>
                                    </tr>

                                    <!-- Alert Deleted -->
                                    <div class="modal fade" id="deletedModal-<?php echo $s['id']?>" tabindex="-1" aria-labelledby="deletedModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deletedModalLabel">Estás seguro que desea elimiar?</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                Estos cambios no pueden ser devueltos.
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Salir</button>
                                                <a type="button" class="btn btn-outline-danger" href="index.php?id=<?php echo $s['id']?>"><i class="bi bi-trash"></i> Eliminar</a>
                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="text-center">
                            <h5>No existen datos para mostrar</h5>
                        </div>
                    <?php } ?>
                </div>
                <div class="card-footer">
                    <a type="button" class="btn btn-warning" href="importador.php"><i class="bi bi-plus-circle"></i> Importar Socios</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>