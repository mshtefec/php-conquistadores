<?php

    if (isset($_POST["import"])) {
        
        $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
        
        if(in_array($_FILES["file"]["type"], $allowedFileType)) {

            $targetPath = __DIR__ . '\\temp\\' . date('d-m-Y_H_i_s') . '_' . $_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

            if ( $xlsx = SimpleXLSX::parse( $targetPath ) ) {

                $sql = "INSERT INTO puntos (
                    concepto, 
                    id_socio, 
                    region, 
                    distri, 
                    linea, 
                    objetivo, 
                    avance_ccc, 
                    avance_por, 
                    peso, 
                    puntos, 
                    ranking_mes, 
                    ranking_acum,
                    id_supervisor, 
                    id_kam, 
                    id_kas, 
                    fecha,
                    fecha_carga
                ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                $stmt = $conn->prepare($sql);
                
                $stmt->bindParam( 1, $concepto);
                $stmt->bindParam( 2, $id_socio);
                $stmt->bindParam( 3, $region);
                $stmt->bindParam( 4, $distri);
                $stmt->bindParam( 5, $linea);
                $stmt->bindParam( 6, $objetivo);
                $stmt->bindParam( 7, $avance_ccc);
                $stmt->bindParam( 8, $avance_por);
                $stmt->bindParam( 9, $peso);
                $stmt->bindParam( 10, $puntos);
                $stmt->bindParam( 11, $ranking_mes);
                $stmt->bindParam( 12, $ranking_acum);
                $stmt->bindParam( 13, $id_supervisor);
                $stmt->bindParam( 14, $id_kam);
                $stmt->bindParam( 15, $id_kas);
                $stmt->bindParam( 16, $fecha);
                $stmt->bindParam( 17, $fecha_carga);

                // voy a quitar la primera fila que son las cabeceras.
                $rows = $xlsx->rows();
                unset($rows[0]);
                
                foreach ( $rows as $key => $fields ) {

                    $concepto = $fields[0];
                    $id_socio = (int) $fields[1];
                    $region = $fields[2];
                    $distri = $fields[3];
                    $linea = $fields[4];
                    $objetivo = $fields[5];
                    $avance_ccc = $fields[6];
                    $avance_por = $fields[7];
                    $peso = $fields[8];
                    $puntos = $fields[9];
                    $ranking_mes = $fields[10];
                    $ranking_acum = $fields[11];
                    $id_supervisor = (int) $fields[12];
                    $id_kam = (int) $fields[13];
                    $id_kas = (int) $fields[14];
                    $fecha = $fields[15];
                    $fecha_carga = date_create('now')->format('Y-m-d H:i:s');
                    
                    try {

                        $log_file .= 'Procesando la linea '. $key . PHP_EOL;

                        $stmt->execute();

                        $type = "success";
                        $message = "Excel Importado Correctamente.";
                    } catch (Exception $e) {
                        echo $e;
                    }
                }

                file_put_contents('logs/log_importador_' . date("Y-m-d") . '.txt', $log_file, FILE_APPEND);
                
            } else {
                echo SimpleXLSX::parseError();
            }
            
        } else { 
            $type = "error";
            $message = "Suba un tipo de archivo invalido Excel.";
        }
    }
    
?>