<?php 

    session_start();

    include('scripts/db.php');

    if(isset($_GET['id'])) {
        $id = $_GET['id'];

        $stmt = $dbh->prepare("
            SELECT 
                *
            FROM 
                socios as s
            WHERE 
                s.id = :id
        ");

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $id = trim($_GET['id']);
        
        $stmt->execute();
        $socio = $stmt->fetch(PDO::FETCH_ASSOC);
    }

?>


<?php include('../index/header.php'); ?>

<?php include('../index/toolbar.php'); ?>

<div class="container-fluid py-4 px-4">
    <div class="p-5 mb-4 bg-light rounded-3">
        <div class="container-fluid py-5">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Detalle de Socio
                    <a type="button" class="btn btn-primary" href="index.php"><i class="bi bi-arrow-return-left"></i> Volver a Socios</a>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Detalle de Socio</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Detalle de Socio</h6>
                    
                    <hr class="my-4">

                    <div class="row g-3">
                        <div class="col-md-4">
                            <label for="id" class="form-label">Codigo</label>
                            <input type="text" class="form-control" id="id" name="id" value="<?php echo $socio['id']?>" readonly>
                        </div>
                        <div class="col-md-4">
                            <label for="region" class="form-label">Region</label>
                            <input type="text" class="form-control" id="region" name="region" value="<?php echo $socio['region']?>" readonly>
                        </div>
                        <div class="col-md-4">
                            <label for="rango" class="form-label">Rango</label>
                            <input type="text" class="form-control" id="rango" name="rango" value="<?php echo $socio['rango']?>" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row g-3">
                        <div class="col-md-3">
                            <label for="nombre" class="form-label">Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $socio['nombre']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="apellido" class="form-label">Apellido</label>
                            <input type="text" class="form-control" id="apellido" name="apellido" value="<?php echo $socio['apellido']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $socio['email']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="clave" class="form-label">Clave</label>
                            <input type="text" class="form-control" id="clave" name="clave" value="<?php echo $socio['clave']?>" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row g-3">
                        <div class="col-md-3">
                            <label for="distribuidora" class="form-label">Distribuidora</label>
                            <input type="text" class="form-control" id="distribuidora" name="distribuidora" value="<?php echo $socio['distribuidora']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="direccion" class="form-label">Direccion</label>
                            <input type="text" class="form-control" id="direccion" name="direccion" value="<?php echo $socio['direccion']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="localidad" class="form-label">Localidad</label>
                            <input type="text" class="form-control" id="localidad" name="localidad" value="<?php echo $socio['localidad']?>" readonly>
                        </div>
                        <div class="col-md-3">
                            <label for="provincia" class="form-label">Provincia</label>
                            <input type="text" class="form-control" id="provincia" name="provincia" value="<?php echo $socio['provincia']?>" readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row g-3">
                        <div class="col-md-4">
                            <label for="telefono" class="form-label">Telefono</label>
                            <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $socio['telefono']?>" readonly>
                        </div>
                    </div>

                    <hr class="my-4">
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('../index/footer.php'); ?>