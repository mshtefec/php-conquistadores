<?php

include_once '../../../php/interfaces/repositoryInterface.php';
include_once '../../../php/models/socio.php';

class SocioRepository implements RepositoryInterface
{
    protected $db;

    public function __construct(Database $db)
    {
        $this->db = $db;
    }

    public function find($id)
    {
        return $this->db->find($id, 'socios', 'Socio');
    }

    public function save(Socio $socio)
    {
        $this->db->save($socio, 'socios');
    }

    public function remove(Socio $socio)
    {
        $this->db->remove($socio, 'socios');
    }
}