<?php 
    include_once("../../../php/class/configuracion.php");

    class Conexion {
        private $host = DDBBHOST;
        private $dbname = DDBBDATABASE;
        private $user = DDBBUSER;
        private $pass = DDBBPASSWORD;
        private $locale = 'es_AR';
        private $connect;

        public function __construct() {
            $connectionString = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname . ';charset=utf8';

            try {
                $this->connect = new PDO($connectionString, $this->user, $this->pass, [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET lc_time_names='" . $this->locale . "'" 
                ]);
                $this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch(PDOException $e) {
                $this->connect = 'Error de conexión.';
                echo "<br>ERROR: " . $e->getMessage();
            }
            
        }

        public function connect() {
            return $this->connect;
        }
    }
?>