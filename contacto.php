<?php
require_once 'php/seguridad.php';

if(!isset($_SESSION)) {session_start();}
?>
<!doctype html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v4.0.1">
		<title>Conquistadores</title>
		<!--link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/"-->
		<!-- Bootstrap core CSS -->
		<link href="assets/dist/css/bootstrap.css" rel="stylesheet">
		<!-- Custom styles for this template -->
		<link href="css/peniaflor.css" rel="stylesheet">
	</head>
	<body>
		<?php 
		include_once('cabecera.php');
		?>
		<main role="main" style="margin-bottom: 4.3rem;">
			
			<section class="gp-balance">
				<div class="container balance">
					<h2>Contacto</h2>
					<div class="row gp-contacto">
						<div class="col-lg-6">
							<img src="img/ases2.png" alt="Instagram" width="42" style=" width: 27rem;" />
						</div>
						<div class="col-lg-6">
							<div class="text-left">
								<h2>¡Bienvenido!</h2>
								<p>Te invitamos a seguirnos en<br><img src="img/icon_instagram.svg" alt="Instagram" width="18"/>
									<a href="https://www.instagram.com/conquistadordelpdv/">@conquistadordelpdv</a> 
								</p>
						 		<p> para mantenerte al tanto de las novedades.</p>
							</div>
						</div>
					</div>
					<!-- /.row -->
				</div><!-- /.container -->
			</section>
		</main>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
		<script src="assets/dist/js/bootstrap.bundle.js"></script>

		<?php include_once('pie.php');?>

		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171840062-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-171840062-1');
		</script>
	</body>
</html>
